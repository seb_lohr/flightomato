#include <math.h>     
#include <iomanip>       
#include <unistd.h>   // usleep()

#include "ms5611.h"
#include "spi.h"
#include "timefunctions.h"

Timefkt baroTime;


MS5611::MS5611(){
  SpiOpenBaro();
}

void MS5611::Initialize(bool shallCheckPROM) {
  Reset();
  if (!ReadPROM()) {
    Reset();
    ReadPROM();
  }
  printf("Baro Initialized\n");
}

void MS5611::SetBaroOSR(int OSR) {
  switch(OSR){
    case 256:
      pressureOSR = CONVERT_D1_OSR_256;
      temperatureOSR = CONVERT_D2_OSR_256;
      conversionTime = 600000 + 10000; // 0.60 ms + 0.01ms safety
    break;
    case 512:
      pressureOSR = CONVERT_D1_OSR_512;
      temperatureOSR = CONVERT_D2_OSR_512;
      conversionTime = 1170000 + 10000; // 1.17 ms + 0.01ms safety
    break;
    case 1024:
      pressureOSR = CONVERT_D1_OSR_1024;
      temperatureOSR = CONVERT_D2_OSR_1024;
      conversionTime = 2280000 + 10000; // 2.28 ms + 0.01ms safety
    break;
    case 2048:
      pressureOSR = CONVERT_D1_OSR_2048;
      temperatureOSR = CONVERT_D2_OSR_2048;
      conversionTime = 4540000 + 10000; // 4.54 ms + 0.01ms safety
    break;
    case 4096:
      pressureOSR = CONVERT_D1_OSR_4096;
      temperatureOSR = CONVERT_D2_OSR_4096;
      conversionTime = 9040000 + 10000; // 9.04 ms + 0.01ms safety
    break;
    default:
      printf("Passed not existing Over Samoling Ratio to Barometer\n");
      exit(1);
    break;
  }
  // printf("conversionTime: %i\n",conversionTime );
}

void MS5611::Reset() {
  int status = SpiWriteBaroCommand(RESET);
  if (status < 0) {
    std::perror("MS5611: could not Reset device");
  }
  // wait for device to be ready
  usleep(10000);
}

bool MS5611::ReadPROM() {
  uint8_t buffer[2];
  for (int i = 0; i < 8; ++i) {
    int status = SpiReadBaro(PROM_READ | (i<<1), buffer, 2); // << = multiplication by 2
    // std::cout << "status " << status << std::endl;
    if (status < 0) {
      return false;
    }
    RegisterFusion(buffer, &C[i], 1);
    // printf("%i\n",C[i] );
  }
  return true;
}

void MS5611::Calibrate(){
  if(firstTime){
    // printf("\nBarometer is calibrating...");
    // fflush( stdout );
    StartBaro();
    for(int i=0;i<2;i++){ // Get one pressure value to set the old pressure value oldP in nerby range for pressure value validation
      ReadBaro();  
      usleep(conversionTime/1000);           
    }
    oldP = P;
    firstTime = false;
  }else{
    if (ReadBaro()){
      calibrationPressure = calibrationPressure*0.7 + P*0.3;
    }
  }
  // printf("calibration pressure: %f\r",(float)calibrationPressure/100 );
}

void MS5611::ConvertPressure(uint8_t pressureOSR) {
  int status = SpiWriteBaroCommand(pressureOSR);
  if (status < 0) {
    std::perror("MS5611: failed to send conversion command");
  }
}

void MS5611::ConvertTemperature(uint8_t temperatureOSR) {
  int status = SpiWriteBaroCommand(temperatureOSR);
  if (status < 0) {
    std::perror("MS5611: failed to send conversion command");
  }
}

void MS5611::ReadADCPressure() {
  uint8_t buffer[3];
  int status = SpiReadBaro(ADC_READ, buffer, 3);
  if (status < 0) {
    std::perror("MS5611: failed to send read command");
  }
  RegisterFusion(buffer,&D1,1);
  // printf("pressure:%i\n",D1 );
}

void MS5611::ReadADCTemperature() {
  uint8_t buffer[3];
  int status = SpiReadBaro(ADC_READ, buffer, 3);
  if (status < 0) {
    std::perror("MS5611: failed to send read command");
  }
  RegisterFusion(buffer,&D2,1);
  // printf("temperature:%i\n",D2 );
}

void MS5611::StartBaro(){
  ConvertPressure(pressureOSR); // start pressure conversion
  state = PRESSURE;
  baroTime.SetTime();
  usleep(conversionTime/1000);
}

bool MS5611::ReadBaro(){
    ValuesReady = false;
    switch (state) {
      case PRESSURE:
        // printf("statePRESSURE");
        if (baroTime.GetTimePassed() < conversionTime){
          // printf("pressure still converting\n");
          break;                                          // not time yet
        }
        ReadADCPressure();
        // printf("read pressure\n");
        ConvertTemperature(temperatureOSR); // start temperature conversion
        // std::cout << "convertTemperature" << std::endl;
        state = TEMPERATURE;
        baroTime.SetTime();
      break;

      case TEMPERATURE:
        if (baroTime.GetTimePassed() < conversionTime){
          // printf("temperature still converting\n");
          break;
        }
        ReadADCTemperature();
        // std::cout << "readADCTemperature" << std::endl;

        ConvertPressure(pressureOSR); // start pressure conversion
        // std::cout << "convertPressure" << std::endl;
        state = PRESSURE;
        baroTime.SetTime();
        
        if(Calculate()){
                  // printf("calculated\n");
          // printf("Temp: %f, pressure: %f\n", m_temperature, m_pressure);
          // std::cout << "ValuesReady = true" << std::endl;
          ValuesReady = true;
          timestamp = (double)baroTime.Getlooptime() / 1000000000; //timestemp of when fifo data was collected in seconds
        }
      break;
    }
  // printf("ValuesReady: %i\n",ValuesReady );
  return ValuesReady;
}

bool MS5611::Calculate() {
  dT = (int32_t)D2 - ((int32_t)C[5] << 8);
  TEMP = 2000 + ((dT * C[6]) >> 23);
  OFF = ((int64_t)C[2] << 16) + (((int64_t)C[4] * dT) >> 7);
  SENS = ((int64_t)C[1] << 15) + ((int64_t)(C[3] * dT) >> 8);
  
  //second order temperature comprensation
  T2 = 0;
  OFF2 = 0;
  SENS2 = 0;
  if (TEMP < 2000) {
      T2 = (dT * dT) >> 31 ;
      OFF2 = 5 * ((TEMP - 2000) * (TEMP - 2000)) / 2;
      SENS2 = OFF2 / 2;
      if (TEMP < -1500) {
          OFF2 += 7 * (TEMP + 1500) * (TEMP + 1500);
          SENS2 += 11 * ((TEMP + 1500) * (TEMP + 1500)) / 2;
      }
      TEMP -= T2;
      OFF -= OFF2;
      SENS -= SENS2;
  } 

  P = ((((int64_t)D1 * SENS) >> 21) - OFF) >> 15;

  if(abs(P - oldP) > 1000){
    // printf("fail P:%i\n",P );
    return false;
  }
  oldP = P;
  // printf("P:%i\n",P );

  temperature = TEMP / 100;

  if(P == calibrationPressure){
    altitude = 0;
  }else{
    altitude = 44330.76923 * (1 - pow(((float)P/100.0)/((float)calibrationPressure/100.0), (float)0.190295)); // Internationale Höhenformel: h = (288,15/0.0065)*(1-P/1013)^(1/5.255))
  }
  
  return true;
}