// #include <stdlib.h>     
// #include <type_traits>
//#include <ios>                // std::ios::failure
#include <math.h> 				//pow()
#include <iostream>				//cout 
#include <cstring>              // std::memset
#include <cstdio>               // std::perror
#include <iomanip>       

#include <fcntl.h>              // open
#include <linux/spi/spidev.h>   // struct spi_ioc_transfer
#include <sys/ioctl.h>          // ioctl
#include <unistd.h>             // close, usleep
#include <cstdint>
#include <time.h>
#include "ms5611.h"
#include "../SPI/spi.h"
#include "../GFFTflight/gfft.h"
// #include "../FlightomatoData.h"

Timefkt baroTime;

MS5611::MS5611(int OSR) {
  SpiOpenPort(0);
  Reset();
  switch(OSR){
    case 256:
      pressureOSR = CONVERT_D1_OSR_256;
      temperatureOSR = CONVERT_D2_OSR_256;
      conversionTime = 600000 + 10000; // 0.60 ms + 0.01ms safety
    break;
    case 512:
      pressureOSR = CONVERT_D1_OSR_512;
      temperatureOSR = CONVERT_D2_OSR_512;
      conversionTime = 1170000 + 10000; // 1.17 ms + 0.01ms safety
    break;
    case 1024:
      pressureOSR = CONVERT_D1_OSR_1024;
      temperatureOSR = CONVERT_D2_OSR_1024;
      conversionTime = 2280000 + 10000; // 2.28 ms + 0.01ms safety
    break;
    case 2048:
      pressureOSR = CONVERT_D1_OSR_2048;
      temperatureOSR = CONVERT_D2_OSR_256;
      conversionTime = 4540000 + 10000; // 4.54 ms + 0.01ms safety
    break;
    case 4096:
      pressureOSR = CONVERT_D1_OSR_4096;
      temperatureOSR = CONVERT_D2_OSR_4096;
      conversionTime = 9040000 + 10000; // 9.04 ms + 0.01ms safety
    break;
    default:
      printf("Passed not existing Over Samoling Ratio to Barometer\n");
      exit(1);
    break;
  }
  printf("conversionTime: %i\n",conversionTime );
}

void MS5611::Initialize(bool shallCheckPROM) {
  SpiOpenPort(0);
  Reset();
  if (!ReadPROM()) {
    Reset();
    ReadPROM();
  }
  printf("\nBarometer initialized");
  Calibrate();
}

void MS5611::Reset() {
	int status = SpiWriteBaroCommand(RESET);
	if (status < 0) {
		std::perror("MS5611: could not Reset device");
	}
	// wait for device to be ready
	usleep(10000);
}

bool MS5611::ReadPROM() {
  uint8_t buffer[2];
  for (int i = 0; i < 8; ++i) {
    int status = SpiReadBaro(PROM_READ | (i<<1), buffer, 2); // << = multiplication by 2
    // std::cout << "status " << status << std::endl;
    if (status < 0) {
      return false;
    }
    RegisterFusion(buffer, &C[i], 1);
    // printf("%i\n",C[i] );
  }
  return true;
}

void MS5611::Calibrate(){
  printf("\nBarometer is calibrating\n");
  StartBaro();
  for(int i=0;i<300;i++){ // scip first Baro Readings for warming up
    ReadBaro();  
    usleep(4540);           
  }

  for (int i=0 ; i<1000 ; i++){ // get 10 pressure Readings
    if (ReadBaro()){
      calibrationPressure = calibrationPressure*0.7 + P*0.3;
    }
    usleep(4540);
  }
  printf("calibration pressure: %f\n",(float)calibrationPressure/100 );
}

void MS5611::ConvertPressure(uint8_t pressureOSR) {
	int status = SpiWriteBaroCommand(pressureOSR);
	if (status < 0) {
		std::perror("MS5611: failed to send conversion command");
	}
}

void MS5611::ConvertTemperature(uint8_t temperatureOSR) {
	int status = SpiWriteBaroCommand(temperatureOSR);
	if (status < 0) {
		std::perror("MS5611: failed to send conversion command");
	}
}

void MS5611::ReadADCPressure() {
  uint8_t buffer[3];
	int status = SpiReadBaro(ADC_READ, buffer, 3);
	if (status < 0) {
		std::perror("MS5611: failed to send read command");
	}
  RegisterFusion(buffer,&D1,1);
  // printf("pressure:%i\n",D1 );
}

void MS5611::ReadADCTemperature() {
  uint8_t buffer[3];
  int status = SpiReadBaro(ADC_READ, buffer, 3);
  if (status < 0) {
    std::perror("MS5611: failed to send read command");
  }
  RegisterFusion(buffer,&D2,1);
  // printf("temperature:%i\n",D2 );
}

void MS5611::StartBaro(){
  ConvertPressure(pressureOSR); // start pressure conversion
	std::cout << "convertPressure" << std::endl;
  state = PRESSURE;
  clock_gettime(CLOCK_REALTIME, &time);
  m_timer = time.tv_nsec;
  usleep(conversionTime/1000);
}

bool MS5611::ReadBaro(){
    ValuesReady = false;
    switch (state) {
      case PRESSURE:
        // printf("statePRESSURE");
        clock_gettime(CLOCK_REALTIME, &time);
        diff = time.tv_nsec - m_timer;
        if ((diff) < conversionTime){
          // printf("pressure still converting\n");
        	break;                                          // not time yet
        }
        ReadADCPressure();
        // printf("read pressure\n");
        ConvertTemperature(temperatureOSR); // start temperature conversion
        // std::cout << "convertTemperature" << std::endl;
        state = TEMPERATURE;
        clock_gettime(CLOCK_REALTIME, &time);
        m_timer = time.tv_nsec;
      break;

      case TEMPERATURE:
        clock_gettime(CLOCK_REALTIME, &time);
        diff = time.tv_nsec - m_timer;
        if ((diff) < conversionTime){
          // printf("temperature still converting\n");
          break;
        }
		    ReadADCTemperature();
		    // std::cout << "readADCTemperature" << std::endl;

        ConvertPressure(pressureOSR); // start pressure conversion
        // std::cout << "convertPressure" << std::endl;
        state = PRESSURE;
        clock_gettime(CLOCK_REALTIME, &time);
        m_timer = time.tv_nsec;
        
        if(Calculate()){
          // printf("Temp: %f, pressure: %f\n", m_temperature, m_pressure);
          // std::cout << "ValuesReady = true" << std::endl;
          ValuesReady = true;
        }
      break;
    }
  // printf("ValuesReady: %i\n",ValuesReady );
  return ValuesReady;
}

bool MS5611::Calculate() {
  dT = (int32_t)D2 - ((int32_t)C[5] << 8);
  TEMP = 2000 + ((dT * C[6]) >> 23);
  OFF = ((int64_t)C[2] << 16) + (((int64_t)C[4] * dT) >> 7);
  SENS = ((int64_t)C[1] << 15) + ((int64_t)(C[3] * dT) >> 8);
  
  //second order temperature comprensation
  T2 = 0;
  OFF2 = 0;
  SENS2 = 0;
  if (TEMP < 2000) {
      T2 = (dT * dT) >> 31 ;
      OFF2 = 5 * ((TEMP - 2000) * (TEMP - 2000)) / 2;
      SENS2 = OFF2 / 2;
      if (TEMP < -1500) {
          OFF2 += 7 * (TEMP + 1500) * (TEMP + 1500);
          SENS2 += 11 * ((TEMP + 1500) * (TEMP + 1500)) / 2;
      }
      TEMP -= T2;
      OFF -= OFF2;
      SENS -= SENS2;
  } 

  P = ((((int64_t)D1 * SENS) >> 21) - OFF) >> 15;

  if(abs(P - oldP) > 1000){
    // printf("fail P:%i\n",P );
    return false;
  }
  oldP = P;

  temperature = TEMP / 100;

  if(P == calibrationPressure){
    altitude = 0;
  }else{
    altitude = 44330.76923 * (1 - pow(((float)P/100.0)/((float)calibrationPressure/100.0), (float)0.190295)); // Internationale Höhenformel: h = (288,15/0.0065)*(1-P/1013)^(1/5.255))
  }
  
  return true;
}

bool MS5611::CheckPROM() {
	/*
	 * if something went wrong it is very likely that all PROM values are zero
	 * but since the crc for this case is valid, we check it separately
	 */
	bool allzero = true;
	for (auto c : C) {
		if (c != 0) {
			allzero = false;
			break;
		}
	}
	if (allzero) {
		// std::cout << "allzero false" << std::endl;
		return false;
	}

	uint16_t res = 0;
	// std::cout << " C[0] " <<C[0]<< " C[1] " <<C[1]<< " C[2] " <<C[2]<< " C[3] " <<C[3]<< " C[4] " <<C[4]<< " C[5] " <<C[5]<< " C[6] " <<C[6]<< " C[7] " <<C[7]<< std::endl;
	uint8_t crc = C[7] & 0xF;
	C[7] &= 0xFF00;
	for (int i = 0; i < 16; ++i) {
		if (i & 1) {
			res ^= ((C[i >> 1]) & 0x00FF);
		} else {
			res ^= ((C[i >> 1]) >> 8);
		}
		for (int j = 0; j < 8; ++j) {
			if (res & 0x8000) {
				res ^= 0x1800;
			}
			res <<= 1;
		}
	}
	C[7] |= crc;

	if (crc == ((res>>12) & 0xF)) {
		// std::cout << "crc true " << std::endl;
		return true;
	}
	// std::cout << "checkPROM false" << std::endl;
	return false;
}

