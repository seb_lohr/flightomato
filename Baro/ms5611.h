#ifndef MS5611_HPP
#define MS5611_HPP

/*----------------------------------------------------------------------
MS5611 - Commands
----------------------------------------------------------------------*/
constexpr uint8_t RESET 			  = 0x1E;

constexpr uint8_t CONVERT_D1_OSR_256  = 0x40;
constexpr uint8_t CONVERT_D1_OSR_512  = 0x42;
constexpr uint8_t CONVERT_D1_OSR_1024 = 0x44;
constexpr uint8_t CONVERT_D1_OSR_2048 = 0x46;
constexpr uint8_t CONVERT_D1_OSR_4096 = 0x48;

constexpr uint8_t CONVERT_D2_OSR_256  = 0x50;
constexpr uint8_t CONVERT_D2_OSR_512  = 0x52;
constexpr uint8_t CONVERT_D2_OSR_1024 = 0x54;
constexpr uint8_t CONVERT_D2_OSR_2048 = 0x56;
constexpr uint8_t CONVERT_D2_OSR_4096 = 0x58;

constexpr uint8_t ADC_READ         		= 0x00;
constexpr uint8_t PROM_READ          	= 0xA0;

/*----------------------------------------------------------------------
States of PressureBackground()
----------------------------------------------------------------------*/
#define READY              0
#define TEMPERATURE        1
#define PRESSURE           2

/*----------------------------------------------------------------------
Oversampling Ratio setting to pass to ReadBaro()
----------------------------------------------------------------------*/
#define ADC_OSR_256 256  
#define ADC_OSR_512 512   
#define ADC_OSR_1024 1024 
#define ADC_OSR_2048 2048 
#define ADC_OSR_4096 4096 

class MS5611 {
public:
	float altitude;
	int32_t P;
	int32_t calibrationPressure = 1;
	double timestamp;

	MS5611();
	void Reset();
	void Initialize(bool shallCheckPROM = true);
	void SetBaroOSR(int OSR);
	bool isInitialized();
	void Setup();
	void Calibrate();

	void ConvertTemperature(uint8_t temperatureOSR);
	void ConvertPressure(uint8_t pressureOSR);

	void ReadADCTemperature();
	void ReadADCPressure();
	bool Calculate();
	void Refresh();
	void ReadADC();
	void StartBaro();
	bool ReadBaro();
	void PressureBackground();

	bool readPROM(bool shallCheckPROM = true);
	bool ReadPROM();

private:
	enum axis {X,Y,Z};
	int state;
	uint32_t conversionTime;
	bool ValuesReady;	


	// ADC values
	uint32_t D1, D2;

	/// PROM variables
	uint16_t C[8];

	uint8_t pressureOSR;
	uint8_t temperatureOSR;

	int32_t oldP;

	int32_t presureDrift;
  int64_t dT;
  int32_t TEMP;
  int64_t OFF;
  int64_t SENS;

  int64_t T2;
  int64_t OFF2;
  int64_t SENS2;

	/// calculated temperature in degree celsius
	float temperature;
	/// calculated pressure in milli bar (i.e. hecto pascal)
	float pressure;
	bool firstTime = true;
};

#endif // MS5611_HPP
