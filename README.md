FLIGHTOMATO - a flightcontroller for a tilt rotor UAS using Raspberry Pi Zero and PXFmini. 

We developed the flightcontroller for our Bachelor's thesis. It has been tested on a quadrocopter frame with servomotors tilting the motors as well as on the hybrid UAS of a helium ballon with four motors with propellers and servos tilting them.

The library might also be used to just access the sensor data of the PXFmini (http://docs.erlerobotics.com/brains/pxfmini) or as reference for your own hardware communication. 


COMPILE-PROCESS:
Type 'make' to compile the code. You might have to change the path to the toolchain (https://github.com/raspberrypi/tools) in the Makefile if you are cross-compiling or compiling natively. 
The executable 'flightomato' is saved in the folder 'Executable'.

CALIBRATION:
Before flying you need to calibrate the sensors! Type 'make calibration' to compile the executable 'calibration', saved in the same location as the flightomato executable. Follow the instructions in the terminal for the calibration process. It is absolutely necessary to calibrate the magnetometer, gyroscope and accelerometer calibration is highly recommended for better flight performance. The information from the calibration is saved in the flightomato.ini file on the same folder level as the executables.

HARDWARE:
Connect the ESCs for the motors to the PWM output channels 1-4 of the PXFmini and the servos to channels 5-8. We used the Spektrum AR7700 RC receiver, connected to the PPM-Sum input channel. The PXFmini and Raspberry Pi can be powered over the usb-hub of the raspberry pi, the PPM-Sum input Rail or the power module hub of the PXFmini. 
On the Raspberry Pi, SPI and I2C communication needs to be enabled aswell as the pigpio.h (https://github.com/joan2937/pigpio) installed. 


More detailled information will follow!

For questions, contact:
hans.ledwa@googlemail.com
sebastianlohr@googlemail.com