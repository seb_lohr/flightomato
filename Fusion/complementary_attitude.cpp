#include "complementary_attitude.h"

 AttitudeFusion::AttitudeFusion(){
    kp_acc = 0.001;
    beta = 0.01;
    EuliCounter = 0;
 }


void AttitudeFusion::calculateEuli(){
    attiEuli[X]= (atan2(2.0 * (q_lg.v[Y] * q_lg.v[Z] + q_lg.s * q_lg.v[X]),1 - 2.0 * (q_lg.v[X] * q_lg.v[X] + q_lg.v[Y] * q_lg.v[Y]))) * MATH_RAD_TO_DEG;
    attiEuli[Y]= (asin(2.0 * (q_lg.s * q_lg.v[Y] - q_lg.v[X] * q_lg.v[Z]))) * MATH_RAD_TO_DEG;
    
    attiEuli[Z] = (atan2(2.0 * (q_lg.v[X] * q_lg.v[Y] + q_lg.s * q_lg.v[Z]),1 - 2.0 * (q_lg.v[Y] * q_lg.v[Y] + q_lg.v[Z] * q_lg.v[Z]))) * MATH_RAD_TO_DEG;

    // float EuliZ = (atan2(2.0 * (q_lg.v[X] * q_lg.v[Y] + q_lg.s * q_lg.v[Z]),1 - 2.0 * (q_lg.v[Y] * q_lg.v[Y] + q_lg.v[Z] * q_lg.v[Z]))) * MATH_RAD_TO_DEG;
    // if ((EuliZ - EuliPrev) > 10){
    //     EuliCounter--;
    // }
    // else if ((EuliZ - EuliPrev) < -10){
    //     EuliCounter++;
    // }
    // attiEuli[Z] = EuliZ + 360.0 * EuliCounter;
    // EuliPrev = EuliZ;
}

 void AttitudeFusion::predict(double dt, float gyr_l[3]){

    //calculate measured angular rate as quaternion from last state
    q_w = quaternions_multiply(q_lg, quaternions_create_from_vector(gyr_l));

    //calculate new state
    for (uint8_t i=0; i<3; i++){
        q_lg.v[i] += q_w.v[i] * dt * 0.5;   
    }
    q_lg.s+=q_w.s * dt * 0.5;   
    q_lg=quaternions_normalise(q_lg);

    // printf("%f\n", dt );
    // printf("%+.4f\t",atan2(2.0 * (q_lg.v[Y] * q_lg.v[Z] + q_lg.s * q_lg.v[X]),1 - 2.0 * (q_lg.v[X] * q_lg.v[X] + q_lg.v[Y] * q_lg.v[Y])) * MATH_RAD_TO_DEG);
    // printf("%+.4f\t",asin(2.0 * (q_lg.s * q_lg.v[Y] - q_lg.v[X] * q_lg.v[Z])) * MATH_RAD_TO_DEG);
    // printf("%+.4f\n",atan2(2.0 * (q_lg.v[X] * q_lg.v[Y] + q_lg.s * q_lg.v[Z]),1 - 2.0 * (q_lg.v[Y] * q_lg.v[Y] + q_lg.v[Z] * q_lg.v[Z])) * MATH_RAD_TO_DEG);
 }



 void AttitudeFusion::updateAccel(float acc[3]){


     //Calculate adaptive gain
    float gain = fabs(vectors_norm(acc)-1.0);
    if (gain < 0.15){
        alpha = kp_acc;
    }
    else if (gain > 0.3){
        return;
    }
    else{
        alpha = (0.3 - gain)/0.15 * kp_acc;
    }
    //Normalize local acceleration and magnetometer vector
    vectors_normalize(acc, acc_l);
    float g[3];
    quaternions_rotate_vector(q_lg, acc_l, g);

    // Calculate delta quaternion between measured acceleration and gravity vector
    dq_acc.s = maths_fast_sqrt((g[Z] + 1) * 0.5);
    dq_acc.v[X] = g[Y] / (2 * dq_acc.s);
    dq_acc.v[Y] = -g[X] / (2 * dq_acc.s);
    dq_acc.v[Z] = 0;

    //Apply filter
    if (dq_acc.s > 0.9){
        dq_acc.s = (1 - alpha) + alpha * dq_acc.s;
        for (uint8_t i=0;i<3;i++){
            dq_acc.v[i] *= alpha;
        }

    } else{
        float om = acos(dq_acc.s);
        float om_1 = sin(om);
        float om_2 = sin((1 - alpha) * om);
        float om_3 = sin(alpha * om);
        dq_acc.s = om_2 / om_1 + om_3 / om_1 * dq_acc.s;
        for (uint8_t i=0;i<3;i++){
            dq_acc.v[i] *= om_3 / om_1;
        }
    }
    dq_acc = quaternions_normalise(dq_acc);
    
    //Apply correction
    q_lg = quaternions_multiply(dq_acc, q_lg);

    // printf("pitch: %+.4f ",atan2(2.0 * (q_lg.v[Y] * q_lg.v[Z] + q_lg.s * q_lg.v[X]),1 - 2.0 * (q_lg.v[X] * q_lg.v[X] + q_lg.v[Y] * q_lg.v[Y])) * MATH_RAD_TO_DEG);
    // printf("\t roll: %+.4f ",asin(2.0 * (q_lg.s * q_lg.v[Y] - q_lg.v[X] * q_lg.v[Z])) * MATH_RAD_TO_DEG);
    // printf("\t yaw: %+.4f \n",atan2(2.0 * (q_lg.v[X] * q_lg.v[Y] + q_lg.s * q_lg.v[Z]),1 - 2.0 * (q_lg.v[Y] * q_lg.v[Y] + q_lg.v[Z] * q_lg.v[Z])) * MATH_RAD_TO_DEG);

 }




 void AttitudeFusion::updateMagnet(float mag[3]){

    vectors_normalize(mag, mag_l);

    quaternions_rotate_vector(q_lg, mag_l, m);

    // printf("%+.4f \t %+.4f \t %+.4f\t", m[X], m[Y], m[Z]);
    // printf("%+.4f \t %+.4f \t %+.4f\t", mag[X], mag[Y], mag[Z]);

    float tmp1 = m[X]*m[X]+m[Y]*m[Y];
    float tmp2 = maths_fast_sqrt(tmp1);
    float tmp3 = 0.5 + m[Y]/(2*tmp2);
    float tmp4 = 2*(m[Y] * tmp2+ tmp1);

    //Quaternion from magnetometer readings
    if(tmp3>0 && tmp4>0){
        dq_mag.s = maths_fast_sqrt(tmp3);
        dq_mag.v[X] = 0;
        dq_mag.v[Y] = 0;
        // dq_mag.v[Z] = maths_fast_sqrt(1-m_tmp);
        dq_mag.v[Z] = m[X]/maths_fast_sqrt(tmp4);
    }

    // printf("%+.4f \t %+.4f \t %+.4f \t %+.4f\n", dq_mag.s, dq_mag.v[X], dq_mag.v[Y], dq_mag.v[Z]);

    //Apply Filter
    if (dq_mag.s > 0.9){
        dq_mag.s = (1 - beta) + beta * dq_mag.s;
        for (uint8_t i=0;i<3;i++){
            dq_mag.v[i] *= beta;
        }
    }
    else{
        float om = acos(dq_mag.s);
        float om_1 = sin(om);
        float om_2 = sin((1 - beta) * om);
        float om_3 = sin(beta * om);
        dq_mag.s = om_2 / om_1 + om_3 / om_1 * dq_acc.s;
        for (uint8_t i=0;i<3;i++){
            dq_mag.v[i] *= om_3 / om_1;
        }
    }

    dq_mag = quaternions_normalise(dq_mag);

    //Apply correction
    q_lg = quaternions_multiply(dq_mag, q_lg);

 }



 void AttitudeFusion::Ini(float acc[3],float mag[3], float PitchStartingAngle, float RollStartingAngle, float YawStartingAngle){
    
    // float cy = cos(YawStartingAngle * 0.5);
    // float sy = sin(YawStartingAngle * 0.5);
    // float cr = cos(-RollStartingAngle * 0.5);
    // float sr = sin(-RollStartingAngle * 0.5);
    // float cp = cos(-PitchStartingAngle * 0.5);
    // float sp = sin(-PitchStartingAngle * 0.5);

    // q_lg.s = cy * cr * cp + sy * sr * sp;
    // q_lg.v[X] = cy * sr * cp - sy * cr * sp;
    // q_lg.v[Y] = cy * cr * sp + sy * sr * cp;
    // q_lg.v[Z] = sy * cr * cp - cy * sr * sp;


    //Normalize local acceleration and magnetometer vector
    // vectors_normalize(acc, acc_l);
    // vectors_normalize(mag, mag_l);
    // // printf("%+.4f \t %+.4f \t %+.4f\n", mag_l[X], mag_l[Y], mag_l[Z]);
    // // printf("%+.4f \t %+.4f \t %+.4f\n", acc_l[X], acc_l[Y], acc_l[Z]);
    // //Quaternion from accelerometer readings
    // if (acc_l[Z]>=0){
    //     dq_acc.s = maths_fast_sqrt((1 + acc_l[Z]) * 0.5);
    //     dq_acc.v[X] = - acc_l[Y] / (2.0 * dq_acc.s);
    //     dq_acc.v[Y] = acc_l[X] / (2.0 * dq_acc.s);
    //     dq_acc.v[Z] = 0;
    // } 
    // else {
    //     dq_acc.v[X] = maths_fast_sqrt((1 - acc_l[Z]) * 0.5);
    //     dq_acc.s = -acc_l[Y] / (2 * dq_acc.v[X]);
    //     dq_acc.v[Y] = 0;
    //     dq_acc.v[Z] = acc_l[X] / (2 * dq_acc.v[X]);
    // }

    // //rotate magnetic field vector to align z-axis with accelerometer z-axis
    // quaternions_rotate_vector(dq_acc, mag_l, m);
    // float l_tmp = m[X] * m[X] + m[Y] * m[Y];
    // float l_sqrt = maths_fast_sqrt(l_tmp);
    // float l_2sqrt = maths_fast_sqrt(2*l_tmp);

    // //Quaternion from magnetometer readings
    // if (m[X]>=0){
    //     dq_mag.s = maths_fast_sqrt(l_tmp + m[X] * l_sqrt) / l_2sqrt;
    //     dq_mag.v[X] = 0;
    //     dq_mag.v[Y] = 0;
    //     dq_mag.v[Z] = m[Y]/(SQRT_2*maths_fast_sqrt(l_tmp + m[X] * l_sqrt));
    // }
    // else{
    //     dq_mag.s = m[Y]/(SQRT_2*maths_fast_sqrt(l_tmp - m[X] * l_sqrt));
    //     dq_mag.v[X] = 0;
    //     dq_mag.v[Y] = 0;
    //     dq_mag.v[Z] = maths_fast_sqrt(l_tmp - m[X] * l_sqrt) / l_2sqrt;
    // }

    
    // q_lg = quaternions_multiply(dq_mag,dq_acc);
 }
