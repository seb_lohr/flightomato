#ifndef COMPLEMENTARY_ATTITUDE_H
#define	COMPLEMENTARY_ATTITUDE_H

#include "constants.hpp"
#include <quaternions.h>
#include <iostream>



class AttitudeFusion {
public:
	AttitudeFusion();
	void Ini(float acc[3],float mag[3], float PitchStartingAngle, float RollStartingAngle, float YawStartingAngle);
 	void predict(double dt, float gyr_l[3]);
	void updateAccel(float acc[3]);
	void updateMagnet(float mag[3]);
	void calculateEuli();

	float attiEuli[3];
	quat_t q_lg;

private:


	quat_t q_w;
	quat_t dq_acc;
	quat_t dq_mag;

	float alpha;
	float beta;

	float kp_acc;

	float acc_l[3];
	float mag_l[3];
	float gyr_l[3];

	float m[3];
	float g[3];

	int EuliCounter;
	float EuliPrev;

};


#endif // _COMPLEMENTARY_H