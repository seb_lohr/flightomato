#include <unistd.h>
#include <iostream>

#include "constants.hpp"
#include "complementary_altitude.h"
#include "vectors.h"



#define SIGMA_ACCEL 0.0886
#define SIGMA_BARO 1.091


AltitudeFusion::AltitudeFusion(){
	altitude = 0;
	velocity = 0;

	gain_velo = sqrt(2 * SIGMA_ACCEL/SIGMA_BARO);
	gain_accel = SIGMA_ACCEL/SIGMA_BARO;
	gain_bias = 0.000000000025;
}

void AltitudeFusion::predict(float acc_l[3], quat_t q_lg, float dt){



	float tmpv[3];
 //rotate acceleration vector to global frame
    quaternions_rotate_vector(q_lg, acc_l, tmpv);

 //substract gravitation and convert to SI-units
 	globalAccel[X] = 9.81 * (tmpv[X] - accel_bias[X]);
 	globalAccel[Y] = 9.81 * (tmpv[Y] - accel_bias[Y]);
 	globalAccel[Z] = 9.81 * (tmpv[Z] - 1 - accel_bias[Z]);

 //calculate speed and velocity from equation of motion
	altitude += dt * (velocity + globalAccel[Z] * 0.5 * dt); //
	velocity += globalAccel[Z] * dt; 

	testcounter++;

	if (testcounter==1000){
		testcounter=0;
		altitude_test1=altitude;
		velocity_test1=velocity;
		altitude_test=altitude;
		velocity_test=velocity;
	}
	altitude_test1 += dt * (velocity + globalAccel[Z] * 0.5 * dt); //
	velocity_test1 += globalAccel[Z] * dt; 

	altitude_test += dt * (velocity_test + 9.81 * (tmpv[Z] - 1) * 0.5 * dt);
	velocity_test += 9.81 * (tmpv[Z] - 1) * dt;
	// printf("%f\n",dt );
    // printf("%+2.4f \t %+2.4f \t %+2.4f \n", altitude, velocity, globalAccel[Z]);
 }

void AltitudeFusion::updateBaro(float baroAltitude, quat_t q_lg, float dt){
	//Calculate difference between predicted altitude and barometer altitude
	float tmp1 = -(altitude - baroAltitude);
	// printf("%+.3f \t %+.3f \t %+.3f\n",accel_bias[Z], barotest, altitude);


	//Correct altitude and velocity with appropriate gain factors
	altitude += (dt * 0.5 * gain_accel + gain_velo) * dt * tmp1;
	velocity += gain_accel * dt * tmp1;
    // printf("%+2.3f \t %+2.3f \t %+2.3f \t %+2.3f \t %+2.3f \n", altitude, velocity, globalAccel[Z], barotest, tmptest);
	// printf("%f\n",dt );

    //Update bias 
    float tmpv1[3] = {};
    float tmpv2[3];
    tmpv1[Z] = tmp1;

    //Rotate to local frame
    quaternions_rotate_vector(quaternions_inverse(q_lg), tmpv1, tmpv2);

    //calculate Bias
    accel_bias[X] -= tmpv2[X] * gain_bias * 2 / (dt * dt);
    accel_bias[Y] -= tmpv2[Y] * gain_bias * 2 / (dt * dt);
    accel_bias[Z] -= tmpv2[Z] * gain_bias * 2 / (dt * dt);

    //Limit Bias
    if (fabs(accel_bias[X])>0.01){
    	if (accel_bias[X]>0.01){
    		accel_bias[X]=0.01;
    	}
    	else if (accel_bias[X]<-0.01){
    		accel_bias[X]=-0.01;
    	}
    }
    if (fabs(accel_bias[Y])>0.01){
    	if (accel_bias[Y]>0.01){
    		accel_bias[Y]=0.01;
    	}
    	else if (accel_bias[Y]<-0.01){
    		accel_bias[Y]=-0.01;
    	}    	
    }
    if (fabs(accel_bias[Z])>0.01){
    	if (accel_bias[Z]>0.01){
    		accel_bias[Z]=0.01;
    	}
    	else if (accel_bias[Z]<-0.01){
    		accel_bias[Z]=-0.01;
    	}
    }
    // printf("%+2.5f \t %+2.5f \t %+2.5f\n", accel_bias[X], accel_bias[Y], accel_bias[Z]);
}







