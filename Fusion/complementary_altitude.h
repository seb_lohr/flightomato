#ifndef COMPLEMENTARY_H
#define	COMPLEMENTARY_H

#include "quaternions.h"


class AltitudeFusion{
public:
	AltitudeFusion();
	void predict(float acc_l[3], quat_t q_lg, float dt);
	void updateBaro(float baroAltitude, quat_t q_lg, float dt);

	float velocity;
	float altitude;
	float globalAccel[3];
	float altitude_test;
	float velocity_test;
	float altitude_test1;
	float velocity_test1;
	int testcounter;

private:

	float gain_accel;
	float gain_velo;
	float gain_bias;
	float accel_bias[3];


	
};


#endif // 