#ifndef SPI_H
#define SPI_H

#define BIGENDIAN true
#define LITTLEENDIAN false

void SpiOpenMPU();
void SpiOpenBaro();

void SpiWriteSingleByteMPU(uint8_t registerAddress, uint8_t data);
int SpiSingleByteReadMPU(uint8_t registerAddress);
void SpiMultiByteReadMPU(uint8_t registerAddress, uint8_t *read, int Bytes );
int SpiTransfer(int spi_device, uint8_t *write,  uint8_t *read, int numberOfWrites, int numberOfReads);

int SpiWriteBaroCommand(uint8_t command);
int SpiReadBaro(uint8_t command, uint8_t *readbuffer, int bytes);

void RegisterFusion(uint8_t *eightBitData, int16_t *sixteenBitData, int nfusions, bool bigEndian);
void RegisterFusion(uint8_t *eightBitData, uint16_t *sixteenBitData, int nfusions);
void RegisterFusion(uint8_t *eightBitData, uint32_t *thirtyBitData, int nfusions);
#endif // SPI_H