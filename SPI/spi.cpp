#include <fcntl.h> 
#include <sys/ioctl.h>     
#include <linux/spi/spidev.h> 
#include <unistd.h>   
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cstring>
#include <pigpio.h>
#include "spi.h"

using namespace std;

int fdMPU;
int fdBaro;
unsigned char spiMode;
unsigned char spiBitsPerWord;
unsigned int spiSpeed;

void SpiOpenMPU(){
    spiMode = SPI_MODE_0;
    spiBitsPerWord = 8;
    spiSpeed = 1000000;    //1000000 = 1MHz (1uS per bit) 

    fdMPU = open("/dev/spidev0.1", O_RDWR);

    if( ioctl(fdMPU, SPI_IOC_WR_MODE, &spiMode) < 0){
      perror("Fail set SPI_IOC_WR_MODE");
    }


    if( ioctl(fdMPU, SPI_IOC_RD_MODE, &spiMode) < 0){
      perror("Fail set SPI_IOC_RD_MODE");
    }

    if( ioctl(fdMPU, SPI_IOC_WR_BITS_PER_WORD, &spiBitsPerWord) < 0){
      perror("Fail set SPI_IOC_WR_BITS_PER_WORD");
    }

    if( ioctl(fdMPU, SPI_IOC_WR_MAX_SPEED_HZ, &spiSpeed) < 0){
      perror("Fail set SPI_IOC_WR_MAX_SPEED_HZ");
    }
}

void SpiOpenBaro(){
    spiMode = SPI_MODE_0;
    spiBitsPerWord = 8;
    spiSpeed = 19000000;    //1000000 = 1MHz (1uS per bit) 

    fdBaro = open("/dev/spidev0.0", O_RDWR);

    if( ioctl(fdBaro, SPI_IOC_WR_MODE, &spiMode) < 0){
      perror("Fail set SPI_IOC_WR_MODE");
    }

    if( ioctl(fdBaro, SPI_IOC_RD_MODE, &spiMode) < 0){
      perror("Fail set SPI_IOC_RD_MODE");
    }

    if( ioctl(fdBaro, SPI_IOC_WR_BITS_PER_WORD, &spiBitsPerWord) < 0){
      perror("Fail set SPI_IOC_WR_BITS_PER_WORD");
    }

    if( ioctl(fdBaro, SPI_IOC_WR_MAX_SPEED_HZ, &spiSpeed) < 0){
      perror("Fail set SPI_IOC_WR_MAX_SPEED_HZ");
    }
}

void SpiWriteSingleByteMPU(uint8_t registerAddress, uint8_t data){
  uint8_t write[2] = {registerAddress, data};
  uint8_t read = 0;
  SpiTransfer(1, write, &read, 2, 0);
}
 
int SpiSingleByteReadMPU(uint8_t registerAddress){
  uint8_t write = registerAddress|0x80;
  uint8_t read = 0;
  SpiTransfer(1, &write, &read, 1, 1);
  return read;
}

void SpiMultiByteReadMPU(uint8_t registerAddress, uint8_t *read, int Bytes ){
  uint8_t write = registerAddress|0x80;
  SpiTransfer(1, &write, read, 1, Bytes);
}

int SpiWriteBaroCommand(uint8_t command){
  uint8_t read = 0;
  return SpiTransfer(0, &command, &read, 1, 0);
}

int SpiReadBaro(uint8_t command, uint8_t *readBuffer, int bytes){
  return SpiTransfer(0, &command, readBuffer, 1, bytes);
}

int SpiTransfer(int spiDevice, uint8_t *write,  uint8_t *read, int numberOfWrites, int numberOfReads){
  struct spi_ioc_transfer spi[2];
  int returnValue = -1;

  std::memset(spi, 0, sizeof(spi));

  spi[0].tx_buf = (unsigned long)write; 
  spi[0].len    = numberOfWrites; 
  spi[1].rx_buf = (unsigned long)read; 
  spi[1].len    = numberOfReads; 

  if(spiDevice)
    returnValue = ioctl(fdMPU, SPI_IOC_MESSAGE(2), spi) ;
  else
    returnValue = ioctl(fdBaro, SPI_IOC_MESSAGE(2), spi) ;
  
  if(returnValue < 0){
    perror("Spi Error:");
  }
  return returnValue;
}

void RegisterFusion(uint8_t *eightBitData, int16_t *sixteenBitData, int nfusions, bool bigEndian){
  if(bigEndian){
    for(int i=0; i<nfusions; i++) {
        sixteenBitData[i] = (int16_t)(((uint16_t)eightBitData[i*2] << 8) | (uint16_t)eightBitData[i*2+1]); 
    }
  }else{
    for(int i=0; i<nfusions; i++) {
        sixteenBitData[i] = (int16_t)(((uint16_t)eightBitData[i*2+1] << 8) | (uint16_t)eightBitData[i*2]); 
    }
  }
}

void RegisterFusion(uint8_t *eightBitData, uint16_t *sixteenBitData, int nfusions){
    for(int i=0; i<nfusions; i++) {
        sixteenBitData[i] = ((uint16_t)eightBitData[i*2] << 8) | eightBitData[i*2+1]; 
    }
}

void RegisterFusion(uint8_t *eightBitData, uint32_t *thirtyBitData, int nfusions){
    for(int i=0; i<nfusions; i++) {
        thirtyBitData[i] = ((uint32_t)eightBitData[i*3] << 16) | eightBitData[i*3+1] << 8 | eightBitData[i*3+2]; 
    }
}