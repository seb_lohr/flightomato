#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <time.h>

#include "timefunctions.h"

using namespace std;

extern uint16_t ppm[8];

Timefkt::Timefkt(){}

Timefkt::Timefkt(long int a){
    targettime=a;
}


// sets current time as reference GetTimePassed
void Timefkt::SetTime(){
    clock_gettime(CLOCK_MONOTONIC, &time);
    oldtime = time.tv_nsec;
}


// calculates time since last SetTime function call
uint64_t Timefkt::GetTimePassed(){
    clock_gettime(CLOCK_MONOTONIC, &time);
    uint64_t passedTime = time.tv_nsec - oldtime;
    if (passedTime<0){
        passedTime+=1000000000;
    }
    return passedTime;
}

// Equalizes the looptime of the main loop to targettime set in constructor
void Timefkt::Timeequalize(){
    clock_gettime(CLOCK_MONOTONIC, &time);
    iterationtime = time.tv_nsec - oldtime;
    if (iterationtime<0){
        iterationtime+=1000000000;
    }
    if (iterationtime <  targettime){
        time.tv_sec = 0;
        time.tv_nsec = targettime - 80000 - iterationtime;
        // nanosleep(&time, (struct timespec *)NULL);
        clock_nanosleep(CLOCK_MONOTONIC  , 0, &time, NULL);
    }
    clock_gettime(CLOCK_MONOTONIC , &time);
    long int lptime = time.tv_nsec - oldtime;
    if (lptime<0){
        lptime+=1000000000;
    }
    oldtime = time.tv_nsec;
    
}

//calculates the time passed since last function call of Getlooptime
int Timefkt::Getlooptime(){
    clock_gettime(CLOCK_MONOTONIC, &time);
    looptime = time.tv_nsec - lastlooptime;
    if (looptime<0){
        looptime+=1000000000;
    }
    lastlooptime = time.tv_nsec;
    // cout << looptime << endl;
    return looptime;
}

//calculates the looptime and prints the time need for one loop if it exceeds the length of all prior looptimes
void Timefkt::Getmaxlooptime(){
    n+=1;
    clock_gettime(CLOCK_MONOTONIC, &time);
    looptime = time.tv_nsec - lastlooptime;
    if (looptime<0){
        looptime+=1000000000;
    }
    lastlooptime = time.tv_nsec;
    // cout << looptime << endl;
    
    if(looptime>maxlooptime && n > 100){
        maxlooptime = looptime;
        cout << "maxlooptime: " << maxlooptime << endl;
    }
}