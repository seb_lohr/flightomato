#ifndef GFFT_H
#define GFFT_H

#include "PCA9685.h"

class Timefkt{
public:
	Timefkt();
	Timefkt(long int a);
	void SetTime();
	uint64_t GetTimePassed();
	void Timeequalize();
	int Getlooptime();
	void Getmaxlooptime();
private:
	int iterationtime;
	long int oldtime;
	long int lastlooptime;
    struct timespec time;
	long int looptime;
	long int maxlooptime;
	int n;
	long int targettime;

};

#endif //GFFT_H