#include <iostream>

#include "timefunctions.h"
#include "Control.h"

using namespace std;

#define RADTODEG 57.2957795    

#define MAX_SERVO_TILT 25 // in deg

#define OUTMAX 95
#define OUTMIN -95
#define IMAX 10
#define IMIN -10

enum axis{X,Y,Z};

extern uint16_t ppm[8];

//Constructor to set kp, ki and kd of pid calculation 
PID::PID(float p, float i, float d){
    kp=p;
    ki=i;
    kd=d;
    firsttime=true;
    ePrior = 0;
}

//Calculates the output of the PID controller
void PID::CalcPID(float y, double dt){
    e = w - y;
    i += e;
    if(i > IMAX){i= IMAX;}
    else if(i < IMIN){i= IMIN;}
    d = (e-ePrior)/dt;
    output = kp*e + ki*i + kd*d;
    if (output > OUTMAX) output = OUTMAX;
    else if (output < OUTMIN) output = OUTMIN;
    ePrior = e;
    // printf("p:%5.5f d:%5.5f dt:%5.5f \n",e,d,dt);
}

//Sets the reference for the PID calculation to the passed value
void PID::Refset(float ref){
    w=ref;
}

//changes the reference for the PID calculation by the passed value
void PID::Refchange(float ref){
    w-=ref;
    // printf("%f\n", w);
}

//returns the reference value of the PID calculation
float PID::GetrefValue(){
    return w;
}

//Function to tune the kp, ki, kd values with your RC
void PID::PidTune(){
    if(ppm[6] < 1700 && ppm[6] > 1300 ){
        return;
    }else if(ppm[6] > 1700){
        if(ppm[7] > 1700){
            kp += kp * 0.00025;
        }else if(ppm[7] < 1700 && ppm[7] > 1300){
            ki += ki * 0.00025; 
        }else if(ppm[7] < 1300){
            kd += kd * 0.00025; 
        }
        printf("%f %f %f \n",kp,ki,kd);
    }else if(ppm[6] < 1300){
        if(ppm[7] > 1700){
            kp -= kp * 0.00025;
        }else if(ppm[7] < 1700 && ppm[7] > 1300){
            ki -= ki * 0.00025; 
        }else if(ppm[7] < 1300){
            kd -= kd * 0.00025; 
        }
        printf("%f %f %f \n",kp,ki,kd);
    }
}

//contstructor to calculate maximum tilt angles
control::control(){
    maxTiltFactor = tan(MAX_SERVO_TILT / RADTODEG); 
    servoScale = (float)116 / 45 * RADTODEG;
    maxTrottleScale = (float)4096 * 0.8 / 20000;
}

//Converts the RC input to scaled values for the control calculation
void control::RcConvert(){
    upwardCommand = ((float)ppm[2] - 1000.1) * maxTrottleScale + 20; 
    float tmp = maxTiltFactor * upwardCommand;
    forwardCommand = tmp * (ppm[0] - 1500) / 500;
    sidewardCommand = tmp * (ppm[1] - 1500) / 500;

    // printf("%+.1f \t %+.1f \t %+.1f \n", upwardCommand, forwardCommand, sidewardCommand);
}

//Calculates the output for the ESCs and Servos
void control::CalcPWM(float pidPitch, float pidRoll, float pidYaw, float pidAltitude, float voltage){
    float Fx[2];
    float Fy[2]; 
    float Fz[4];

    // Fx[0] = - forwardCommand + pidYaw;
    // Fx[1] = - forwardCommand - pidYaw;

    // Fy[0] = -sidewardCommand + pidYaw;
    // Fy[1] = -sidewardCommand - pidYaw;

    // Fz[0] = upwardCommand + pidPitch + pidAltitude;
    // Fz[1] = upwardCommand - pidPitch + pidAltitude;
    // Fz[2] = upwardCommand - pidRoll + pidAltitude;
    // Fz[3] = upwardCommand + pidRoll + pidAltitude;


    Fx[0] = - forwardCommand + pidYaw;
    Fx[1] = - forwardCommand - pidYaw;

    Fy[0] = -sidewardCommand + pidYaw;
    Fy[1] = -sidewardCommand - pidYaw;

    Fz[0] = upwardCommand + pidPitch;
    Fz[1] = upwardCommand - pidPitch;
    Fz[2] = upwardCommand - pidRoll;
    Fz[3] = upwardCommand + pidRoll;

    // printf("%+.3f \t %+.3f \t %+.3f\n", pidYaw,Fy[0],Fz[0]);

    pulseWidth[0] = 288 - atan(Fy[1]/Fz[1]) * servoScale;       //Calculate PWM signal for rear servo 
    pulseWidth[1] = 308 + atan(Fy[0]/Fz[0]) * servoScale;       //Calculate PWM signal for front servo [204,404] 
    pulseWidth[2] = 320 + atan(Fx[0]/Fz[2]) * servoScale;       //Calculate PWM signal for right servo 
    pulseWidth[3] = 310 - atan(Fx[1]/Fz[3]) * servoScale;       //Calculate PWM signal for left servo 

    pulseWidth[4] = 204 + sqrt(Fy[1] * Fy[1] + Fz[1] * Fz[1]); //Calculate the PWM signal for rear motor (CCW)  
    pulseWidth[5] = 204 + sqrt(Fy[0] * Fy[0] + Fz[0] * Fz[0]); //Calculate the PWM signal for front motor (CCW) 
    pulseWidth[6] = 204 + sqrt(Fx[0] * Fx[0] + Fz[2] * Fz[2]);  //Calculate the PWM signal for right motor (CW) 
    pulseWidth[7] = 204 + sqrt(Fx[1] * Fx[1] + Fz[3] * Fz[3]);;  //Calculate the PWM signal for left motor (CW) 

    if(pulseWidth[0]>=368){
        pulseWidth[0]=368;
    }else if(pulseWidth[0]<=208){
        pulseWidth[0]=208;
    }
    if(pulseWidth[1]>=386){
        pulseWidth[1]=386;
    }else if(pulseWidth[1]<=226){
        pulseWidth[1]=226;
    }
    if(pulseWidth[2]>=400){
        pulseWidth[2]=400;
    }else if(pulseWidth[2]<=240){
        pulseWidth[2]=240;
    }
    if(pulseWidth[3]>=390){
        pulseWidth[3]=390;
    }else if(pulseWidth[3]<=230){
        pulseWidth[3]=230;
    }

    // printf("%i %i %i %i \n",pulseWidth[0],pulseWidth[1],pulseWidth[2],pulseWidth[3]);

    // pulseWidth[0] = 288 + sidewardCommand;
    // pulseWidth[1] = 308 + sidewardCommand;
    // pulseWidth[2] = 320 - forwardCommand;
    // pulseWidth[3] = 310 - forwardCommand;
    // pulseWidth[4] = 204 + upwardCommand;
    // pulseWidth[5] = 204 + upwardCommand;
    // pulseWidth[6] = 204 + upwardCommand;
    // pulseWidth[7] = 204 + upwardCommand;

    pulseWidth[0] = 288;
    pulseWidth[1] = 308;
    pulseWidth[2] = 320;
    pulseWidth[3] = 310;

    pulseWidth[4] = 204;
    pulseWidth[5] = 204;
    // pulseWidth[6] = 254;
    // pulseWidth[7] = 254;

//Compensate the voltage drop of the batteries (factor t.b.d.)
    // if (voltage < 12.4 && voltage > 8){                   
    //   pulseWidth[4] += pulseWidth[4] * ((12.4 - voltage)/factor);              
    //   pulseWidth[5] += pulseWidth[5] * ((12.4 - voltage)/factor);              
    //   pulseWidth[6] += pulseWidth[6] * ((12.4 - voltage)/factor);              
    //   pulseWidth[7] += pulseWidth[7] * ((12.4 - voltage)/factor;              
    // }

    // printf("P1:[%d] P2:[%d] R1:[%d] R1:[%d] P1:[%d] P2:[%d] R1:[%d] R1:[%d] \n",pulseWidth[0],pulseWidth[1],pulseWidth[2],pulseWidth[3],pulseWidth[4],pulseWidth[5],pulseWidth[6],pulseWidth[7]);
}

//Sets all channels on the PWM-rail to 1ms high time 
void control::SetLowPWM(){
    for(int i=0; i<4; i++){
        pulseWidth[i] = 204; //81  
    }
    for(int i=4; i<8; i++){
        pulseWidth[i] = 204;   //81
    }
}

//Sets all channels on the PWM-rail to 2ms high time 
void control::SetHighPWM(){
    for(int i=0; i<4; i++){
        pulseWidth[i] = 409;   //491
    }
    for(int i=4; i<8; i++){
        pulseWidth[i] = 409;   //491
    }
}

