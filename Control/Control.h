#ifndef CONTROL_H
#define CONTROL_H

#include "maths.h"

class PID{
  
  float i;
  long int dt;
  float ePrior;
  uint16_t tuneswitch;
  bool firsttime;
  int counter;

public:
  PID(float p, float i, float d);

  float w;
  float e;
  float d;

  float kp;
  float ki;
  float kd;
  float output;
  void CalcPID(float y, double dt);
  void PidTune();
  void Refchange(float ref);
  void Refset(float ref);
  float GetrefValue();
};

class control{
    float forwardCommand;
    float sidewardCommand;
    float upwardCommand;
    float rad_factor=180.0/PI*2.088889;
    float maxTiltFactor;
    float servoScale;
    float maxTrottleScale;
public:
    uint16_t pulseWidth[8];
    control();
    void CalcPWM(float pidYaw, float pidbitchgyro, float pidprollgyro, float pidAltitude, float voltage);
    void SetLowPWM();
    void SetHighPWM();
    void RcConvert();
};


#endif //CONTROL_H