#ifndef IMUCALIBRATION_H
#define IMUCALIBRATION_H


bool quitSensor = false;
bool save = false; 
bool print = false;
bool record = true;
bool valuesAreReadIN = false; 

float filteredAccelValue[3];
float accelCalibration[6];
float magCalibration[6];

float magBias[3];
float magScale[3];

float accelSensitivity;
float accelBias[3];
float accelScale[3];

int32_t gyroBias[3];
int32_t gyroScale[3];

float readInValues[21] = {0};

void Calibrate();
char GetUserInput();
void GetGyroBias();
void GetGyroScale();
void CalculateMagCalibration();
void CalculateAccelCalibration();
void Test();
bool ReadIniFile();
void CompareOldNew();
void SaveIniFile();

#endif
