#include <stdio.h>
#include <iostream>
#include <fstream>
#include <pigpio.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/wait.h>

#include <math.h> // fabs
#include "MPU9250.h"
#include "SPI/spi.h"
#include "ImuCalibration.h"
#include "complementary_attitude.h"
#include "timefunctions.h"
#include "userinterface.h"

using namespace std;

MPU9250 imu;
AttitudeFusion fusion;
Userinterface userinterface;

int main(){
   userinterface.DisableConsoleBuffering();

  imu.Initialize(); 

  magCalibration[0] = -32760;
  magCalibration[1] = 32760;
  magCalibration[2] = -32760;
  magCalibration[3] = 32760;
  magCalibration[4] = -32760;
  magCalibration[5] = 32760;


  accelSensitivity = imu.GetAccelSensitivity();

  do{
    Calibrate();
  }while(!save);
  CalculateMagCalibration();
  CalculateAccelCalibration();
  if(ReadIniFile()){
    CompareOldNew();
  }
  SaveIniFile();
  return 0;
}

void Calibrate(){
  int choise = 0;
  char sensor = 0;
  char axis = 0;
  char input = 0; 
  float old[3] = {0}; 

  save = false;
  quitSensor = false;
  print = false;
  record = true;

  printf("\n\n    -----Calibration-----\n");
  printf("    mouting angle on frame  - f \n");
  printf("         Magnetometer       - m \n");
  printf("         Acceleromter       - a \n");
  printf("          Gyroscope         - g \n");
  printf("            Test            - t \n");
  printf("        Save and Quit       - s \n");
  printf("     Quit without saving    - q \n");

  cin >> sensor;

  if(sensor != 'f' && sensor != 'm' && sensor != 'a' && sensor != 'g' && sensor != 't' && sensor != 's' && sensor != 'q' ){
    printf("\n\nunvalid input!\n");
    quitSensor = true;
  }
  
  if(sensor == 'f'){
    if(!imu.GetCalibration()){ 
      quitSensor = true;
    } 
    printf("\nWait until angle converged || quit - q\n");
    imu.EnableFifoWrites(WRITE_GYRO|WRITE_ACCEL); // Enable writing of GYRO and ACCEL measuremets on FIFO. FIFO must be read by function GetAccelGyroData() after max 42 samples to avoid FIFO overflow!
  }else if(sensor == 'm'){
    printf("\nRotate flightcontroller around all axis until extrem values don't change anymore || reset values - r || quit - q\n");
  }else if(sensor == 'a'){
    printf("\nChoose Axis to calibrate x / y / z || reset values - r  || quit - q\n");
  }else if(sensor == 'g'){
    printf("\nGyro Bias Calibration stops automaticly|| quit - q\n");
    GetGyroBias();
    // GetGyroScale();
    quitSensor = true;
  }else if(sensor == 't'){
    CalculateAccelCalibration();
    CalculateMagCalibration();
    Test();
    quitSensor = true;
  }else if(sensor == 'q'){
    exit(1);
  } 

  while(!quitSensor && !save){
    switch(sensor){  
      case 'a':
        imu.ReadAccel();
        switch(axis){
          case 'x':
            choise = 0;
            print = true;
          break;
          case 'y':
            choise = 2;
            print = true;
          break;
          case 'z':
            choise = 4;
            print = true;
          break;
        }
        if(record){
          if(print){
            filteredAccelValue[choise/2] = 0.99 * filteredAccelValue[choise/2] + 0.01 * (float)imu.accelValues[choise/2];
            if(accelCalibration[choise] < filteredAccelValue[choise/2]){
              accelCalibration[choise] = filteredAccelValue[choise/2];
            }else if(accelCalibration[choise+1] > filteredAccelValue[choise/2]){
              accelCalibration[choise+1] = filteredAccelValue[choise/2];
            }
            printf("Accel%cMax: % 1.2f Accel%cMin: % 1.2f\r",axis,accelCalibration[choise]/accelSensitivity,axis,accelCalibration[choise+1]/accelSensitivity);
          }        
        }
      break;

      case 'm':
        usleep(10000);
        if(imu.GetMagData()){
          if(record){ 
            for(int i=0;i<3;i++){
              if(fabs( (old[i] - (float)imu.magValues[i]) ) < 200){     // protect for random high values
                if(magCalibration[i*2] < (float)imu.magValues[i]){
                  magCalibration[i*2] = (float)imu.magValues[i];
                }else if(magCalibration[i*2+1] > (float)imu.magValues[i]){
                  magCalibration[i*2+1] = (float)imu.magValues[i];
                }
                old[i] = (float)imu.magValues[i];
              }
            }                        
          }
          printf("Mag: X %+4.0f %+4.0f [%+3.i] || Y %+4.0f %+4.0f [%+3.i] || Z %+4.0f %+4.0f [%+3.i] \n", magCalibration[0], magCalibration[1], imu.magValues[X], magCalibration[2], magCalibration[3], imu.magValues[Y], magCalibration[4], magCalibration[5], imu.magValues[Z]);
        }
      break;

      case 'f':
        //======================== Read Accelerometer und Gyroscope
        imu.GetAccelGyroData(); // Calles ReadFifo() and saves gyro and accel measuremets with calibration compensation on arrays accel[3] and gyro[3]
        //======================== Update Fusion
        fusion.predict(imu.timestamp, imu.gyro);
        fusion.updateAccel(imu.accel);
        fusion.calculateEuli();
        printf("pitch: %+.4f \t roll: %+.4f\r",fusion.attiEuli[Y],fusion.attiEuli[X]);
        usleep(2000); 
      break;

      case 's':
        save = true;
      break;
    } // switch(sensor)

    if ((input = userinterface.GetConsoleInput()) != 0) {
      switch (input) {
        case 'x':
          axis = 'x';
        break;
        case 'y':
          axis = 'y';
        break;
        case 'z':
          axis = 'z';
        break;
        case 'p':
        if(record){
          record = false;
        }else{
          record = true;
        }
        break;
        case 'q':
          quitSensor = true;
          print = false;
          imu.DisableFifoWrites(); // disable writes to FIFO in case it was enabled for option 'f' to get the mouting angle on frame
        break;
        case 'r':
          if(sensor == 'm'){
            magCalibration[0] = -32760;
            magCalibration[1] = 32760;
            magCalibration[2] = -32760;
            magCalibration[3] = 32760;
            magCalibration[4] = -32760;
            magCalibration[5] = 32760;
          }else if(sensor == 'a'){
            accelCalibration[choise] = 0;
            accelCalibration[choise+1] = 0;
          }
        break;
      }
    }
  } // while(!quitSensor && !save)
} // Calibration()

void CalculateMagCalibration(){
  float magAxisLength[3];
  float magAverageAxisLength;

  if(magCalibration[0] == -32760 && magCalibration[1] == 32760 && magCalibration[2] == -32760 && magCalibration[3] == 32760 && magCalibration[4] == -32760 && magCalibration[5] == 32760){
    return;
  }

   magBias[0]  = (magCalibration[0] + magCalibration[1])/2;
   magBias[1]  = (magCalibration[2] + magCalibration[3])/2;
   magBias[2]  = (magCalibration[4] + magCalibration[5])/2;

   magAxisLength[0]  = (magCalibration[0] - magCalibration[1])/2;
   magAxisLength[1]  = (magCalibration[2] - magCalibration[3])/2;
   magAxisLength[2]  = (magCalibration[4] - magCalibration[5])/2;

   magAverageAxisLength = (magAxisLength[0] + magAxisLength[1] + magAxisLength[2]) / 3.0;
   // printf("magAverageAxisLength: %f\n",magAverageAxisLength);

   magScale[0] = (float)magAxisLength[0]/magAverageAxisLength;
   magScale[1] = (float)magAxisLength[1]/magAverageAxisLength;
   magScale[2] = (float)magAxisLength[2]/magAverageAxisLength;
 }

void CalculateAccelCalibration(){
  if(accelCalibration[0] == 0 && accelCalibration[1] == 0 && accelCalibration[2] == 0 && accelCalibration[3] == 0 && accelCalibration[4] == 0 && accelCalibration[5] == 0){
    return;
  }
  if(accelCalibration[0] != 0 && accelCalibration[1] != 0){
    accelBias[0]  = (accelCalibration[0] + accelCalibration[1])/2; 
    accelScale[0] = (accelCalibration[0] - accelCalibration[1])/(2*accelSensitivity); 
  }
  if(accelCalibration[2] != 0 && accelCalibration[3] != 0){
    accelBias[1]  = (accelCalibration[2] + accelCalibration[3])/2;
    accelScale[1] = (accelCalibration[2] - accelCalibration[3])/(2*accelSensitivity);  
  }
  if(accelCalibration[4] != 0 && accelCalibration[5] != 0){
    accelBias[2]  = (accelCalibration[4] + accelCalibration[5])/2; 
    accelScale[2] = (accelCalibration[4] - accelCalibration[5])/(2*accelSensitivity); 
  }
 }

 void GetGyroBias(){
    int gyroSamples = 500;
    int32_t gyroCheckBias[3] = {0};
    do{
        // set Gyro Offset Registers to 0 to sample new raw gyro values for calibration - if MPU is not resetted offset registers are not 0
        SpiWriteSingleByteMPU( XG_OFFSET_H, 0);
        SpiWriteSingleByteMPU( XG_OFFSET_L, 0);
        SpiWriteSingleByteMPU( YG_OFFSET_H, 0);
        SpiWriteSingleByteMPU( YG_OFFSET_L, 0);
        SpiWriteSingleByteMPU( ZG_OFFSET_H, 0);
        SpiWriteSingleByteMPU( ZG_OFFSET_L, 0);

        usleep(100000);

        gyroBias[X] = 0;
        gyroBias[Y] = 0;
        gyroBias[Z] = 0;

        for(int i=0;i<gyroSamples;i++){
            imu.ReadGyro();
            gyroBias[X] += (int32_t)imu.gyroValues[X];
            gyroBias[Y] += (int32_t)imu.gyroValues[Y];
            gyroBias[Z] += (int32_t)imu.gyroValues[Z];
            // printf("%i %i %i %i \n",i,gyroBias[X]/(i+1),gyroBias[Y]/(i+1),gyroBias[Z]/(i+1));
            // printf("Bias: %i %i %i \n",imu.gyroValues[X],imu.gyroValues[Y],imu.gyroValues[Z]);
        }

        gyroBias[X] /= gyroSamples;
        gyroBias[Y] /= gyroSamples;
        gyroBias[Z] /= gyroSamples;
   
        printf("Gyro Bias Values: %i %i %i ",gyroBias[X],gyroBias[Y],gyroBias[Z]);

        int32_t gyroBiasRegPrep[3];
        gyroBiasRegPrep[X] = - gyroBias[X] / 4 * imu.offsetRegFormatFactor; // OffsetLSB = X_OFFS_USR * 4 / 2^FS_SEL
        gyroBiasRegPrep[Y] = - gyroBias[Y] / 4 * imu.offsetRegFormatFactor; // OffsetLSB = Y_OFFS_USR * 4 / 2^FS_SEL
        gyroBiasRegPrep[Z] = - gyroBias[Z] / 4 * imu.offsetRegFormatFactor; // OffsetLSB = Z_OFFS_USR * 4 / 2^FS_SEL


        uint8_t gyroBias8Bit[6]={0};
        gyroBias8Bit[0] = (gyroBiasRegPrep[X]  >> 8) & 0xFF;
        gyroBias8Bit[1] = (gyroBiasRegPrep[X])       & 0xFF;
        gyroBias8Bit[2] = (gyroBiasRegPrep[Y]  >> 8) & 0xFF;
        gyroBias8Bit[3] = (gyroBiasRegPrep[Y])       & 0xFF;
        gyroBias8Bit[4] = (gyroBiasRegPrep[Z]  >> 8) & 0xFF;
        gyroBias8Bit[5] = (gyroBiasRegPrep[Z])       & 0xFF;

        SpiWriteSingleByteMPU( XG_OFFSET_H, gyroBias8Bit[0]);
        SpiWriteSingleByteMPU( XG_OFFSET_L, gyroBias8Bit[1]);
        SpiWriteSingleByteMPU( YG_OFFSET_H, gyroBias8Bit[2]);
        SpiWriteSingleByteMPU( YG_OFFSET_L, gyroBias8Bit[3]);
        SpiWriteSingleByteMPU( ZG_OFFSET_H, gyroBias8Bit[4]);
        SpiWriteSingleByteMPU( ZG_OFFSET_L, gyroBias8Bit[5]);

        usleep(100000);

        // Check if Gyro Bias Calibration was done successfully 

        gyroCheckBias[X] = 0;
        gyroCheckBias[Y] = 0;
        gyroCheckBias[Z] = 0;

        for(int i=0;i<gyroSamples;i++){
            imu.ReadGyro();
            gyroCheckBias[X] += (int32_t)imu.gyroValues[X];
            gyroCheckBias[Y] += (int32_t)imu.gyroValues[Y]; 
            gyroCheckBias[Z] += (int32_t)imu.gyroValues[Z];
            // printf("Check: %i %i %i \n",gyroCheckBias[X]/(i+1),gyroCheckBias[Y]/(i+1),gyroCheckBias[Z]/(i+1));
            // printf("Check: %i %i %i \n",imu.gyroValues[X],imu.gyroValues[Y],imu.gyroValues[Z]);
        }
        gyroCheckBias[X] /= gyroSamples;
        gyroCheckBias[Y] /= gyroSamples; 
        gyroCheckBias[Z] /= gyroSamples;
        printf("New Bias Values: %i %i %i ",gyroCheckBias[X],gyroCheckBias[Y],gyroCheckBias[Z]);

        if(abs(gyroCheckBias[X]) > 1 || abs(gyroCheckBias[Y]) > 1 || abs(gyroCheckBias[Z]) > 1)
          printf("Bias still to high. Keep Drone still\n");

    }while(abs(gyroCheckBias[X]) > 1 || abs(gyroCheckBias[Y]) > 1 || abs(gyroCheckBias[Z]) > 1);
}

void GetGyroScale(){
    float gyroSum[3] = {0};
    float gyroSum1[3] = {0};
    printf("\nGyro Scale Calibration\n");
    for(int i=0;i<20000;i++){
        imu.ReadGyro();
        gyroSum[X] += (float)imu.gyroValues[X];
        gyroSum[Y] += (float)imu.gyroValues[Y]; 
        gyroSum[Z] += (float)imu.gyroValues[Z];
        printf("%i %f %f %f \n",i,gyroSum[X]/i,gyroSum[Y]/i,gyroSum[Z]/i);
    }
    gyroSum[X] /= 20000;
    gyroSum[Y] /= 20000; 
    gyroSum[Z] /= 20000;
    printf("%f %f %f \n",gyroSum[X],gyroSum[Y],gyroSum[Z]);

    sleep(2);
    
    for(int i=0;i<20000;i++){
        imu.ReadGyro();
        gyroSum1[X] += (float)imu.gyroValues[X]-gyroSum[X];
        gyroSum1[Y] += (float)imu.gyroValues[Y]-gyroSum[Y]; 
        gyroSum1[Z] += (float)imu.gyroValues[Z]-gyroSum[Z];
        printf("%i %f %f %f \n",i,gyroSum1[X]/i,gyroSum1[Y]/i,gyroSum1[Z]/i);
    }
}

void Test(){
    char testchoice;
    bool testing = true;
    bool valuesAreReadIN = false;
      printf("----------------Test calibration--------------\n");
      printf("  Get existing calibration from ini  - e\n");
      printf("Continue with new calibration values - any key \n");
      cin >> testchoice;
      if(testchoice == 'e'){
        ReadIniFile();
        valuesAreReadIN = true;
        magBias[X] = readInValues[0];
        magBias[Y] = readInValues[1];
        magBias[Z] = readInValues[2];
        magScale[X] = readInValues[3];
        magScale[Y] = readInValues[4];
        magScale[Z] = readInValues[5];
        accelBias[X] = readInValues[6];
        accelBias[Y] = readInValues[7];
        accelBias[Z] = readInValues[8];
        accelScale[X] = readInValues[9];
        accelScale[Y] = readInValues[10];
        accelScale[Z] = readInValues[11];
      }
    while(testing){

      printf("\n      Accelerometer       - a\n");
      printf("      Magnetometer        - m\n");
      printf("         Quit             - q\n");
      cin >> testchoice;
      if (testchoice == 'q'){
        if(valuesAreReadIN == true){
          printf("\nCalibration values set zero again\n");
          magBias[X] = 0;
          magBias[Y] = 0;
          magBias[Z] = 0;
          magScale[X] = 0;
          magScale[Y] = 0;
          magScale[Z] = 0;
          accelBias[X] = 0;
          accelBias[Y] = 0;
          accelBias[Z] = 0;
          accelScale[X] = 0;
          accelScale[Y] = 0;
          accelScale[Z] = 0;
        }
        return;
      }
      while( userinterface.GetConsoleInput() != 'q') {  
        switch(testchoice){
          case 'a':
              imu.ReadAccel();
              
              // printf(" %f %f %f %f ",(float)imu.accelValues[X], accelBias[X], accelScale[X], accelSensitivity );
              printf("X: % 1.3f ",((float)imu.accelValues[X] - accelBias[X]) / accelScale[X] / accelSensitivity);
              printf("Y: % 1.3f ",((float)imu.accelValues[Y] - accelBias[Y]) / accelScale[Y] / accelSensitivity);
              printf("Z: % 1.3f \n",((float)imu.accelValues[Z] - accelBias[Z]) / accelScale[Z] / accelSensitivity);
              // printf("X: % 1.3f\n ",(float)imu.accelValues[X] / accelSensitivity);
              // printf("Y: % 1.3f ",(float)imu.accelValues[Y] / accelSensitivity);
              // printf("Z: % 1.3f\n",(float)imu.accelValues[Z] / accelSensitivity);
          break;
          case 'm':
              usleep(1000);
              if(imu.GetMagData()){
                float uT[3];
                // uT[X] = ((float)imu.magValues[X] - magBias[X]) / magScale[X]; 
                printf("%f ",(float)imu.magValues[X] / magScale[X]);
                printf("%f ",(float)imu.magValues[Y] / magScale[Y]);
                printf("%f \n",(float)imu.magValues[Z] / magScale[Z]);
                // uT[Y] = ((float)imu.magValues[Y] - magBias[Y]) / magScale[Y];
                // uT[Z] = ((float)imu.magValues[Z] - magBias[Z]) / magScale[Z];
                // printf("Absolute magnetic field: % 4.3f \r",sqrt( uT[X]*uT[X] + uT[Y]*uT[Y] + uT[Z]*uT[Z] ) );
              }
          break;
        }
      }
    }
 }



void CompareOldNew(){
  if(magBias[X] == 0){
    magBias[X] = readInValues[0];
  }
  if(magBias[Y] == 0){
    magBias[Y] = readInValues[1];
  }
  if(magBias[Z] == 0)
    magBias[Z] = readInValues[2];

  if(magScale[X] == 0){
    magScale[X] = readInValues[3];
  }
  if(magScale[Y] == 0){
    magScale[Y] = readInValues[4];
  }
  if(magScale[Z] == 0){
    magScale[Z] = readInValues[5];
  }

  if(accelBias[X] == 0){
    accelBias[X] = readInValues[6];
  }
  if(accelBias[Y] == 0){
    accelBias[Y] = readInValues[7];
  }
  if(accelBias[Z] == 0){
    accelBias[Z] = readInValues[8];
  }

  if(accelScale[X] == 0){
    accelScale[X] = readInValues[9];
  }
  if(accelScale[Y] == 0){
    accelScale[Y] = readInValues[10];
  }
  if(accelScale[Z] == 0){
    accelScale[Z] = readInValues[11];
  }

  if(gyroBias[X] == 0){
    gyroBias[X] = readInValues[12];
  }
  if(gyroBias[Y] == 0){
    gyroBias[Y] = readInValues[13];
  }
  if(gyroBias[Z] == 0){
    gyroBias[Z] = readInValues[14];
  }

  if(gyroScale[X] == 0){
    gyroScale[X] = readInValues[15];
  }
  if(gyroScale[Y] == 0){
    gyroScale[Y] = readInValues[16];
  }
  if(gyroScale[Z] == 0){
    gyroScale[Z] = readInValues[17];
  }
}


bool ReadIniFile(){
  string line;
  ifstream in; 
  size_t position;
  in.open("flightomato.ini"); 
  if (in.is_open()){
      getline(in,line);           //read first line to skip it
      getline(in,line);           //read second line 
      position = line.find("Successfull");
      if (position == string::npos){
        printf("\n\n\n\nflightomato.ini has no valid format. New ini is created\n");
        return false;
      }
      // printf("\nImu Calibration data from flightomato.ini was found. Calibration values are:\n");
      int n = 0;
      while (getline(in,line)){
        position = line.find (' ');
        string valuestring = line.substr (position);
        readInValues[n] = std::stof (valuestring);
        // printf("calibrationValue%i: %f\n",n,calibrationValue[n]);
        n++;
      }
      printf("\n\n\n\nExisting flightomato.ini found\n");
      return true;
      in.close();
  }else{
    printf("\n\n\n\nNo flightomato.ini found. New ini is created!\n");
    return false;
  }
}


void SaveIniFile(){
  string line;

  // open a file in write mode.
  ofstream out;
  out.open("flightomato.ini");

  out << "----Imu calibration----\n";
  out << "Calibration: Successfull\n";
  
  out << "magBiasX: " << magBias[X] <<"\n";
  out << "magBiasY: " << magBias[Y] <<"\n";
  out << "magBiasZ: " << magBias[Z] <<"\n";
  out << "magScaleX: " << magScale[X] <<"\n";
  out << "magScaleY: " << magScale[Y] <<"\n";
  out << "magScaleZ: " << magScale[Z] <<"\n";

  out << "accelBiasX: " << accelBias[X] <<"\n";
  out << "accelBiasY: " << accelBias[Y] <<"\n";
  out << "accelBiasZ: " << accelBias[Z] <<"\n";
  out << "accelScaleX: " << accelScale[X] <<"\n";
  out << "accelScaleY: " << accelScale[Y] <<"\n";
  out << "accelScaleZ: " << accelScale[Z] <<"\n";

  out << "gyroBiasX: " << gyroBias[X] <<"\n";
  out << "gyroBiasY: " << gyroBias[Y] <<"\n";
  out << "gyroBiasZ: " << gyroBias[Z] <<"\n";

  out << "gyroScaleX: " << gyroScale[X] <<"\n";
  out << "gyroScaleY: " << gyroScale[Y] <<"\n";
  out << "gyroScaleZ: " << gyroScale[Z] <<"\n";

  out << "mountingPitch: " << fusion.attiEuli[Y] <<"\n";
  out << "mountingRitch: " << fusion.attiEuli[X] <<"\n";

   // close the opened file
  out.close();

   // open a file in read mode
  ifstream in; 
  in.open("flightomato.ini"); 

  cout << "Following Calibration Data was written to flightomato.ini" << endl; 

  if (in.is_open()){
    while ( getline (in,line) ){
      cout << line << '\n';
    }
    in.close();
  }else{
    cout << "Unable to open file"; 
    return;
  }

  // close the opened file
  in.close();
  return;
}
