#ifndef MPU9250_H
#define MPU9250_H

/*-------------------------------------------------------------------------------------
MPU9250 - Registers
-------------------------------------------------------------------------------------*/

#define SELF_TEST_X_GYRO   0x00
#define SELF_TEST_Y_GYRO   0x01
#define SELF_TEST_Z_GYRO   0x02
#define SELF_TEST_X_ACCEL  0x0D
#define SELF_TEST_Y_ACCEL  0x0E
#define SELF_TEST_Z_ACCEL  0x0F
#define XG_OFFSET_H        0x13
#define XG_OFFSET_L        0x14
#define YG_OFFSET_H        0x15
#define YG_OFFSET_L        0x16
#define ZG_OFFSET_H        0x17
#define ZG_OFFSET_L        0x18
#define SMPLRT_DIV         0x19
#define CONFIG             0x1A
#define GYRO_CONFIG        0x1B
#define ACCEL_CONFIG       0x1C
#define ACCEL_CONFIG_2     0x1D
#define LP_ACCEL_ODR       0x1E
#define WOM_THR            0x1F
#define FIFO_EN            0x23
#define I2C_MST_CTRL       0x24
#define I2C_SLV0_ADDR      0x25
#define I2C_SLV0_REG       0x26
#define I2C_SLV0_CTRL      0x27
#define I2C_SLV1_ADDR      0x28
#define I2C_SLV1_REG       0x29
#define I2C_SLV1_CTRL      0x2A
#define I2C_SLV2_ADDR      0x2B
#define I2C_SLV2_REG       0x2C
#define I2C_SLV2_CTRL      0x2D
#define I2C_SLV3_ADDR      0x2E
#define I2C_SLV3_REG       0x2F
#define I2C_SLV3_CTRL      0x30
#define I2C_SLV4_ADDR      0x31
#define I2C_SLV4_REG       0x32
#define I2C_SLV4_DO        0x33
#define I2C_SLV4_CTRL      0x34
#define I2C_SLV4_DI        0x35
#define I2C_MST_STATUS     0x36
#define INT_PIN_CFG        0x37
#define INT_ENABLE         0x38
#define INT_STATUS         0x3A
#define ACCEL_XOUT_H       0x3B
#define ACCEL_XOUT_L       0x3C
#define ACCEL_YOUT_H       0x3D
#define ACCEL_YOUT_L       0x3E
#define ACCEL_ZOUT_H       0x3F
#define ACCEL_ZOUT_L       0x40
#define TEMP_OUT_H         0x41
#define TEMP_OUT_L         0x42
#define GYRO_XOUT_H        0x43
#define GYRO_XOUT_L        0x44
#define GYRO_YOUT_H        0x45
#define GYRO_YOUT_L        0x46
#define GYRO_ZOUT_H        0x47
#define GYRO_ZOUT_L        0x48
#define EXT_SENS_DATA_00   0x49
#define EXT_SENS_DATA_01   0x4A
#define EXT_SENS_DATA_02   0x4B
#define EXT_SENS_DATA_03   0x4C
#define EXT_SENS_DATA_04   0x4D
#define EXT_SENS_DATA_05   0x4E
#define EXT_SENS_DATA_06   0x4F
#define EXT_SENS_DATA_07   0x50
#define EXT_SENS_DATA_08   0x51
#define EXT_SENS_DATA_09   0x52
#define EXT_SENS_DATA_10   0x53
#define EXT_SENS_DATA_11   0x54
#define EXT_SENS_DATA_12   0x55
#define EXT_SENS_DATA_13   0x56
#define EXT_SENS_DATA_14   0x57
#define EXT_SENS_DATA_15   0x58
#define EXT_SENS_DATA_16   0x59
#define EXT_SENS_DATA_17   0x5A
#define EXT_SENS_DATA_18   0x5B
#define EXT_SENS_DATA_19   0x5C
#define EXT_SENS_DATA_20   0x5D
#define EXT_SENS_DATA_21   0x5E
#define EXT_SENS_DATA_22   0x5F
#define EXT_SENS_DATA_23   0x60
#define I2C_SLV0_DO        0x63
#define I2C_SLV1_DO        0x64
#define I2C_SLV2_DO        0x65
#define I2C_SLV3_DO        0x66
#define I2C_MST_DELAY_CTRL 0x67
#define SIGNAL_PATH_RESET  0x68
#define MOT_DETECT_CTRL    0x69
#define USER_CTRL          0x6A
#define PWR_MGMT_1         0x6B
#define PWR_MGMT_2         0x6C
#define FIFO_COUNTH        0x72
#define FIFO_COUNTL        0x73
#define FIFO_R_W           0x74
#define WHO_AM_I           0x75
#define XA_OFFSET_H        0x77
#define XA_OFFSET_L        0x78
#define YA_OFFSET_H        0x7A
#define YA_OFFSET_L        0x7B
#define ZA_OFFSET_H        0x7D
#define ZA_OFFSET_L        0x7E

/*-------------------------------------------------------------------------------------
MPU9250 - Bitmasks
-------------------------------------------------------------------------------------*/

// Bitmasks PWR_MGMT_1 Register
#define H_RESET               0b10000000
#define SLEEP                 0b01000000
#define CLKSEL_INTERNAL_20MHZ 0b00000000

// Bitmasks PWR_MGMT_2 Register
#define ENABLE_GYRO_AND_ACCEL 0b00000000

// Bitmasks USER_CTRL Register
#define USER_CTRL_FIFO_EN   0b01000000
#define I2C_MST_EN          0b00100000
#define FIFO_RST            0b00000100
#define I2C_MST_RST         0b00000010

// Bitmasks INT_ENABLE Register
#define FIFO_OVERFLOW_EN 0b00010000

// Bitmasks INT_STATUS Register
#define FIFO_OVERFLOW_INT 0b00010000

// Bitmasks FIFO_EN Register
#define WRITE_TEMP  0b10000000
#define WRITE_GYRO  0b01110000
#define WRITE_ACCEL 0b00001000
#define WRITE_SLV_0 0b00000001
#define DISABLE_WRITES 0b00000000

// Bitmasks CONFIG Register for DLPF (Digital Low-Pass Filter)
#define DLPF_CFG_250HZ         0b00000000
#define DLPF_CFG_184HZ         0b00000001
#define DLPF_CFG_92HZ          0b00000010
#define DLPF_CFG_41HZ          0b00000011
#define DLPF_CFG_20HZ          0b00000100
#define DLPF_CFG_10HZ          0b00000101
#define DLPF_CFG_5HZ           0b00000110
#define DLPF_CFG_3600Hz        0b00000111

// Bitmasks GYRO_CONFIG Register for Gyroscope Full Scale Select
#define BYPASSDLPF_GYRO                 0b00000011
#define GYRO_FS_SEL_250DPS              0b00000000
#define GYRO_FS_SEL_500DPS              0b00001000
#define GYRO_FS_SEL_1000DPS             0b00010000
#define GYRO_FS_SEL_2000DPS             0b00011000

// Bitmasks ACCEL_CONFIG Register for Accelerometer Full Scale Select
#define ACCEL_FS_SEL_2G                  0b00000000
#define ACCEL_FS_SEL_4G                  0b00001000
#define ACCEL_FS_SEL_8G                  0b00010000
#define ACCEL_FS_SEL_16G                 0b00011000
#define ACCEL_FS_SEL_MASK                0b00011000

// Bitmasks ACCEL_CONFIG_2 Register for Accelerometer low pass filter
#define BYPASSDLPF_ACCEL    0b00001000
#define A_DLPFCFG_218P1HZ   0b00000000
#define A_DLPFCFG_218P1HZ_1 0b00000001
#define A_DLPFCFG_99HZ      0b00000010
#define A_DLPFCFG_44P8HZ    0b00000011
#define A_DLPFCFG_21P2HZ    0b00000100
#define A_DLPFCFG_10P2HZ    0b00000101
#define A_DLPFCFG_5P05HZ    0b00000110
#define A_DLPFCFG_420HZ     0b00000111

// Bitmasks I2C_SLV0_ADDR Register
#define AK8963_I2C_ADDR     0x0c // access the AK8963 magnetometer directly from the host. In this configuration the slave address for the AK8963 is 0X0C or 12 decimal
#define TRANSFER_IS_A_READ  0b10000000
#define READ_BITMASK  0b10000000

// Bitmasks I2C_SLV0_CTRL Register
#define ENABLE_READING    0b10000000

/*-------------------------------------------------------------------------------------------
AK8963 - Registers
--------------------------------------------------------------------------------------------*/

#define AK8963_WIA                  0x00
#define AK8963_INFO                 0x01
#define AK8963_ST1                  0x02
#define AK8963_HXL                  0x03
#define AK8963_HXH                  0x04
#define AK8963_HYL                  0x05
#define AK8963_HYH                  0x06
#define AK8963_HZL                  0x07
#define AK8963_HZH                  0x08
#define AK8963_ST2                  0x09
#define AK8963_CNTL1                0x0A
#define AK8963_CNTL2                0x0B
#define AK8963_ASTC                 0x0C
#define AK8963_TS1                  0x0D
#define AK8963_TS2                  0x0E
#define AK8963_I2CDIS               0x0F
#define AK8963_ASAX                 0x10
#define AK8963_ASAY                 0x11
#define AK8963_ASAZ                 0x12

/*-------------------------------------------------------------------------------------
AK8963 - Bitmasks
-------------------------------------------------------------------------------------*/

// Bitmasks AK8963_CNTL1 Register
#define POWER_DOWN_MODE                 0b00000000
#define SINGLE_MEASUREMENT_MODE         0b00000001
#define CONTINUOUS_MEASUREMENT_MODE_1   0b00000010  // 8Hz
#define CONTINUOUS_MEASUREMENT_MODE_2   0b00000110  // 100Hz
#define EXTERNAL_TRIGGER_MEASURMET_MODE 0b00000100
#define SELF_TEST_MODE                  0b00001000
#define FUSE_ROM_ACCESS_MODE            0b00001111
#define OUTPUT_16_BIT                   0b00010000

// Bitmasks AK8963_CNTL2 Register
#define SOFT_RESET 0b00000001

class MPU9250 {
    public:
        float accel[3];
        float gyro[3];
        double timestamp;
        float mag[3];
        bool magUpdated;

        // float magAngle;
        // float magFactoryScaled[3];

        int16_t accelValues[3];
        int16_t gyroValues[3];
        int16_t magValues[3];

        int offsetRegFormatFactor;

        float sensitivityAdjustment[3];
        int16_t NumberFifoSamples;
        int32_t fifoData[9];

        float pitchMountingAngle;
        float rollMountingAngle;

        MPU9250();
        bool GetCalibration();
        void ResetMPU();
        void Initialize();
        void Setup(int configBitmask, int gyroConfigBitmask, int accelConfigBitmask);
        float GetAccelSensitivity();
        void DivideSampleRate(int divider);
        void EnableFifo();
        void EnableFifoWrites(uint8_t bitmask);
        void DisableFifoWrites();
        bool CountFifoSamples();
        bool ReadFifo();
        void GetAccelGyroData();
        bool GetMagData();

        void EnableMag();
        void SetMagInPowerDownMode();
        void SetMagInFuseRomAccessMode();
        void GetMagASAValues();
        void SetMagInContinuosMeasurementMode();
        void EnableReadingFromMag(uint8_t i2cSlaveRegisterAdress, uint8_t nBytes);
        void TestReadMagDeviceIDRegister();
        float WrapAroundAngle(float angle);
        void ReadAccel();
        void ReadGyro();
        uint8_t WhoAmI();

    private:
        enum axis {X,Y,Z};
        
        bool magDataReady = false;
        bool readPrepared = false;

        int bytespersample;
        bool fifoMag = false;
        bool fifoGyro = false;
        bool fifoAccel = false;    

        bool repeat = true;

        float gyroSensitivity = 1;
        float accelSensitivity = 1;
        float magSensitivity = 1;


        float magBias[3] = {0};
        float magScale[3] = {0};

        float accelBias[3] = {0};
        float accelScale[3] = {0};
        float accelAverageAxisLength;

        int32_t gyroBias[3];
        int32_t gyroScale[3];
};

#endif //MPU9250_H
