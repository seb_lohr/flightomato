#include <iostream>
#include <fstream>
#include <unistd.h> //usleep()

#include "MPU9250.h"
#include "spi.h"
#include "quaternions.h"
#include "timefunctions.h" // SetTime(),GetTimePassed()
#include "math.h"

using namespace std;

#define G_SI 9.80665


//-----------------------------------------------------------------------------------------------

Timefkt magTime;
Timefkt imuTime;
Timefkt readMagtime;

/*
Contructor gets called when the object is created. Opens spidev0.1 and sets settings for Kommunikations with MPU-9259
*/
MPU9250::MPU9250(){
    SpiOpenMPU();
}

/* 
Reads in calibrationvalues for the Accelerometer, Gyroscope and Magnetometer from the flightomato.ini file. 
If no flightomato.ini file exist or has a wrong format error message gets printed and program stops running.
*/
bool MPU9250::GetCalibration(){
    string line;
    string valuestring;
    float calibrationValue[21] = {0};
    size_t position;

    ifstream in; 
    in.open("flightomato.ini"); 
    if (in.is_open()){
        getline(in,line);           //read first line to  
        getline(in,line);           //read 
        position = line.find("Successfull");
        if (position == string::npos){
            printf("\nflightomato.ini has no valid calibration values\n"); 
            return false;
        }
        // printf("\nImu Calibration data from flightomato.ini was found. Calibration values are:\n");
        int n = 0;
        while (getline(in,line)){
            position = line.find (' ');
            valuestring = line.substr (position);
            calibrationValue[n] = std::stof (valuestring);
            // printf("calibrationValue%i: %f\n",n,calibrationValue[n]);
            n++;
        }
        in.close();

        magBias[X] = calibrationValue[0];
        magBias[Y] = calibrationValue[1];
        magBias[Z] = calibrationValue[2];
        // printf("magBias Werte: \n");
        // printf("%f\n",magBias[X]);
        // printf("%f\n",magBias[Y]);
        // printf("%f\n",magBias[Z]);

        magScale[X] = calibrationValue[3];
        magScale[Y] = calibrationValue[4];
        magScale[Z] = calibrationValue[5];
        // printf("magScale Werte:\n");
        // printf("%f\n", magScale[X]);
        // printf("%f\n", magScale[Y]);
        // printf("%f\n", magScale[Z]);

        accelBias[X] = calibrationValue[6];
        accelBias[Y] = calibrationValue[7];
        accelBias[Z] = calibrationValue[8];
        // printf("accelBias Werte:\n");
        // printf("%f\n", accelBias[X]);
        // printf("%f\n", accelBias[Y]);
        // printf("%f\n", accelBias[Z]);

        accelScale[X] = calibrationValue[9];
        accelScale[Y] = calibrationValue[10];
        accelScale[Z] = calibrationValue[11];
        // printf("accelScale Werte:\n");
        // printf("%f\n", accelScale[X]);
        // printf("%f\n", accelScale[Y]);
        // printf("%f\n", accelScale[Z]);

        gyroBias[X] = calibrationValue[12];
        gyroBias[Y] = calibrationValue[13];
        gyroBias[Z] = calibrationValue[14];
        // printf("gyroBias Werte:\n");
        // printf("%i\n", gyroBias[X]);
        // printf("%i\n", gyroBias[Y]);
        // printf("%i\n", gyroBias[Z]);

        gyroScale[X] = calibrationValue[15];
        gyroScale[Y] = calibrationValue[16];
        gyroScale[Z] = calibrationValue[17];
        // printf("gyroScale Werte:\n");
        // printf("%i\n", gyroScale[X]);
        // printf("%i\n", gyroScale[Y]);
        // printf("%i\n", gyroScale[Z]);

        pitchMountingAngle = calibrationValue[18];
        rollMountingAngle = calibrationValue[19];
        // printf("mounting angles:\n");
        // printf("pitch: %f\n",pitchMountingAngle);
        // printf("roll: %f\n",rollMountingAngle);

        // Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
        uint8_t gyroBias8Bit[6]; 

        // printf("offsetRegFormatFactor: %i\n",offsetRegFormatFactor );
        gyroBias[X] = - gyroBias[X] / 4 * offsetRegFormatFactor; // OffsetLSB = X_OFFS_USR * 4 / 2^FS_SEL
        gyroBias[Y] = - gyroBias[Y] / 4 * offsetRegFormatFactor; // OffsetLSB = Y_OFFS_USR * 4 / 2^FS_SEL
        gyroBias[Z] = - gyroBias[Z] / 4 * offsetRegFormatFactor; // OffsetLSB = Z_OFFS_USR * 4 / 2^FS_SEL

        gyroBias8Bit[0] = (gyroBias[X]  >> 8) & 0xFF; // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format (OffsetLSB = Y_OFFS_USR * 4 / 2^FS_SEL
        gyroBias8Bit[1] = (gyroBias[X])       & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
        gyroBias8Bit[2] = (gyroBias[Y]  >> 8) & 0xFF;
        gyroBias8Bit[3] = (gyroBias[Y])       & 0xFF;
        gyroBias8Bit[4] = (gyroBias[Z]  >> 8) & 0xFF;
        gyroBias8Bit[5] = (gyroBias[Z])       & 0xFF;

        SpiWriteSingleByteMPU( XG_OFFSET_H, gyroBias8Bit[0]);
        SpiWriteSingleByteMPU( XG_OFFSET_L, gyroBias8Bit[1]);
        SpiWriteSingleByteMPU( YG_OFFSET_H, gyroBias8Bit[2]);
        SpiWriteSingleByteMPU( YG_OFFSET_L, gyroBias8Bit[3]);
        SpiWriteSingleByteMPU( ZG_OFFSET_H, gyroBias8Bit[4]);
        SpiWriteSingleByteMPU( ZG_OFFSET_L, gyroBias8Bit[5]);

        sleep(1);

    }else{
        cout << "\nUnable to open flightomato.ini. Is imu calibrated?\n"; 
        return false;
    }
    in.close();
    printf("\nCalibration file read\n");
    return true;
}

/*
Can be called to reset the internal registers and restores the default settings.
*/
void MPU9250::ResetMPU(){
    SpiWriteSingleByteMPU( PWR_MGMT_1, H_RESET);  // Set BIT[7]=1 in register 107 - Power Management 1. Bit autocears
    usleep(1000000);                              // wait for reset to be done
}

/*
Initializes the MPU-9250.
Need to be called before the MPU can be used as it sets necesarry registers to sample data form the accelerometer, gyroscope and magnetometer.
*/
void MPU9250::Initialize(){
    Setup(DLPF_CFG_184HZ, GYRO_FS_SEL_1000DPS, ACCEL_FS_SEL_8G);    // Pass desired Digital low-pass filter Bandwidth, Gyro Full Scale, Accel Full Scale
    DivideSampleRate(1);                                            // divide MPU sample rate by [1:256]
    EnableFifo();                                                   // Enable 

    EnableMag(); 
  // TestReadMagDeviceIDRegister();
    SetMagInPowerDownMode();
    SetMagInFuseRomAccessMode();
    GetMagASAValues();
    SetMagInPowerDownMode();
    SetMagInContinuosMeasurementMode();
    EnableReadingFromMag(AK8963_ST1,8);
    printf("\nImu initialized\n");
}

void MPU9250::Setup(int configBitmask, int gyroConfigBitmask, int accelConfigBitmask){  
    SpiWriteSingleByteMPU( PWR_MGMT_1, CLKSEL_INTERNAL_20MHZ);  // Select Clock Source: Internal 20MHz oscillator BIT[2:0] value 0
    SpiWriteSingleByteMPU( PWR_MGMT_2, ENABLE_GYRO_AND_ACCEL);  // enable X,Y,Z accelerometer & X,Y,Z gyro BIT[5:0]=0
    SpiWriteSingleByteMPU( USER_CTRL, FIFO_RST);                // Reset FIFO module BIT[2]=1, Reset I2C Master module BIT[1]=1
    usleep(10000);                                              // ????? wait for reset to be done
    SpiWriteSingleByteMPU( INT_ENABLE, FIFO_OVERFLOW_EN);       // Enable interrupt for fifo overflow to propagate to interrupt pin BIT[4]=1
    SpiWriteSingleByteMPU( FIFO_EN, DISABLE_WRITES);            // disable writes to FIFO: BIT[7:0]=0
    SpiSingleByteReadMPU(INT_STATUS);                           // read FIFO overflow interrup. Interrupt status is cleared only by reading INT_STATUS register (register 55, INT_ANYRD_2CLEAR BIT)

    // SpiWriteSingleByteMPU( USER_CTRL, 0b00000000);           // Disable FIFO access from serial interface BIT[6]=0, Disable I2C Master I/F module BIT[5]=0
    
    SpiWriteSingleByteMPU( CONFIG, configBitmask);              // set Digital Low-Pass filter (DLPF) off choise BIT[2:0] (188Hz)
    SpiWriteSingleByteMPU( SMPLRT_DIV, 0);                      // Set sample rate to 1 kHz (Set Sample Rate Diver = 0) - 8-Bit Sample Rate Divider register => possible values [0,255] SAMPLE_RATE= Internal_Sample_Rate / (1 + SMPLRT_DIV) (Page 12)
    SpiWriteSingleByteMPU( GYRO_CONFIG, gyroConfigBitmask);     // Gyro Full Scale Select BIT[4:3]: +250dps(00) +500dps(01) +1000dps(10) +2000dps(11)
    SpiWriteSingleByteMPU( ACCEL_CONFIG, accelConfigBitmask);   // Accel Full Scale Select BIT[4:3]: ±2g(00) ±4g (01) ±8g(10) ±16g(11)

    switch (gyroConfigBitmask){
        case GYRO_FS_SEL_250DPS:
            gyroSensitivity = 131;       // Gyro Sensitivity = 2^16 LSB / 500dps = 131 (Seite 11) => gyro output value = 131 for a 500dps angualar velocity 
            offsetRegFormatFactor = 1;  
        break;
        case GYRO_FS_SEL_500DPS:
            gyroSensitivity = 65.5;      // 131/2
            offsetRegFormatFactor = 2;
        break;
        case GYRO_FS_SEL_1000DPS:
            gyroSensitivity = 32.75;     // 131/4
            offsetRegFormatFactor = 4;
        break;
        case GYRO_FS_SEL_2000DPS:
            gyroSensitivity = 16.375;    // 131/8
            offsetRegFormatFactor = 8;
        break;
    }

    switch (accelConfigBitmask){
        case ACCEL_FS_SEL_2G:
            accelSensitivity = 16384;  //Accel Sensitivitiy = 2^16 / 4 = 16384 => accel output value = 16384 for 2g acceleration
        break;
        case ACCEL_FS_SEL_4G:
            accelSensitivity = 8192;  //16384/2
        break;
        case ACCEL_FS_SEL_8G:
            accelSensitivity = 4096;  //16384/4
        break;
        case ACCEL_FS_SEL_16G:
            accelSensitivity = 2048;  //16384/8
        break;
    }
    // printf("gyroSensitivity: %f accelSensitivity: %f\n",gyroSensitivity,accelSensitivity );
}

float MPU9250::GetAccelSensitivity(){
    return accelSensitivity;
}

void MPU9250::EnableFifo(){
    SpiWriteSingleByteMPU( USER_CTRL, USER_CTRL_FIFO_EN|I2C_MST_EN);    // Enable the I2C Master I/F module; pins ES_DA and ES_SCL are isolated from pins SDA/SDI and SCL/ SCLK BIT[5]=1
    SpiWriteSingleByteMPU( FIFO_EN, DISABLE_WRITES);                    // disable writes to FIFO: BIT[7:0]=0
}

void MPU9250::EnableFifoWrites(uint8_t bitmask){
    switch(bitmask){
        case WRITE_GYRO|WRITE_ACCEL|WRITE_SLV_0:
            bytespersample = 19;
            // printf("bytespersample: %i\n",bytespersample);
        break;
        case WRITE_GYRO|WRITE_ACCEL:
            bytespersample = 12;
            // printf("bytespersample: %i\n",bytespersample);
        break;
        case WRITE_SLV_0:
            bytespersample = 7;
            fifoMag = true;
            fifoGyro = false;
            fifoAccel = false;
        break;
    }
    SpiWriteSingleByteMPU(FIFO_EN,bitmask);  // enable writes to fifo: (GYRO_X BIT[6]=1), GYRO_Y BIT[5]=1, GYRO_Z BIT[4]=1, ACCEL BIT[3]=1, (SLV0 BIT[0]=1)
    if(SpiSingleByteReadMPU(FIFO_EN) != bitmask)
        printf("Fail EnableFifoWrites! FIFO_EN is decimal value %i instead of %i \n",SpiSingleByteReadMPU(FIFO_EN),bitmask);
}

void MPU9250::DisableFifoWrites(){
    SpiWriteSingleByteMPU( FIFO_EN, DISABLE_WRITES);
}

void MPU9250::DivideSampleRate(int divider){
    divider-=1;
    SpiWriteSingleByteMPU(SMPLRT_DIV, divider);
    if(SpiSingleByteReadMPU(SMPLRT_DIV) != divider)
        printf("Fail SampeRateDivide! SMPLRT_DIV is decimal value %i instead of %i \n",SpiSingleByteReadMPU(SMPLRT_DIV),divider);
}

bool MPU9250::CountFifoSamples(){
    uint8_t countData[2];
    int16_t fifoCount;
    
    SpiWriteSingleByteMPU( FIFO_EN, DISABLE_WRITES);        // ???? (seconde time after InitializeForFifo) disable writes to FIFO: BIT[7:0]=0
    
    SpiMultiByteReadMPU( FIFO_COUNTH,countData,2);    // read FIFO_COUNTH & FIFO_COUNTL regsiter => number of written bytes in the FIFO
    RegisterFusion(countData,&fifoCount,1,BIGENDIAN);     // Fusionate high and low bytes of register values into a singend 16-Bit value
    // printf("fifoCount: %i\n", fifoCount);    // Write fifoCount value (number of to FIFO writen bytes) to the standard output
    NumberFifoSamples = fifoCount/bytespersample; // Number of Samples written to FIFO (with 12 bits per sample dependig on number of enabled writes to FIFO (FIFO_EN register))

    // printf("%i\n",NumberFifoSamples );

    if((SpiSingleByteReadMPU(INT_STATUS)&FIFO_OVERFLOW_INT) == FIFO_OVERFLOW_INT){
            switch(fifoCount){
                case 512:
                    printf("FIFO overflow! %i bytes written to FIFO \n", fifoCount);    // Write fifoCount value (number of to FIFO writen bytes) to the standard output 
                    uint8_t emptyFifo[512];
                    SpiMultiByteReadMPU( FIFO_R_W, emptyFifo,512);
                    return false;
                break;
                default:
                    printf("Fifo Overflow Flag is set but written bytes to Fifo only %i. Interrupt status is cleared only by reading INT_STATUS register (option Register 55: INT_ANYRD_2CLEAR) \n",fifoCount);
                    return true;
                break;
            }
    }else if(NumberFifoSamples == 0){
        return false;
    }
    return true;                        
}

bool MPU9250::ReadFifo(){
    uint8_t fifoRegisters[19]={0};
    int16_t sensorValues[9]={0};
    
    // memset(fifoData, 0, sizeof(fifoData));
    for(int i=0;i<bytespersample/2;i++){
        fifoData[i]=0;
    }
    
    if(!CountFifoSamples()){
        return false;
    }
    // printf("nSamples: %i bytespersample: %i\n",nSamples,bytespersample);
    if(NumberFifoSamples == 0){
        printf("0 Samples in FIFO. Probably read before first or next sample\n");
        return false;
    }

    for(int i=0;i<NumberFifoSamples;i++){
        SpiMultiByteReadMPU( FIFO_R_W, fifoRegisters,bytespersample);
        RegisterFusion(fifoRegisters,sensorValues,bytespersample/2,BIGENDIAN);

        // printf("%i %i %i\n",sensorValues[3],sensorValues[4],sensorValues[5]);

        for(int i=0;i<bytespersample/2;i++){
            fifoData[i]+=(int32_t)sensorValues[i];    
        }
    }

    timestamp = (double)imuTime.Getlooptime() / 1000000000; //timestemp of when fifo data was collected in seconds

    for(int i=0;i<bytespersample/2;i++){
        fifoData[i]/=(int32_t)NumberFifoSamples;
    }
    return true;
}

void MPU9250::GetAccelGyroData(){
    if(ReadFifo()){
        accel[X] = -((float)fifoData[0] - accelBias[X]) / accelScale[X] / accelSensitivity;
        accel[Y] = -((float)fifoData[1] - accelBias[Y]) / accelScale[Y] / accelSensitivity;
        accel[Z] = -((float)fifoData[2] - accelBias[Z]) / accelScale[Z] / accelSensitivity;    

        gyro[X] = (float)fifoData[3]/gyroSensitivity*MATH_DEG_TO_RAD; 
        gyro[Y] = (float)fifoData[4]/gyroSensitivity*MATH_DEG_TO_RAD; 
        gyro[Z] = (float)fifoData[5]/gyroSensitivity*MATH_DEG_TO_RAD; 
    }
    EnableFifoWrites(WRITE_GYRO|WRITE_ACCEL);
}

bool MPU9250::GetMagData(){
uint8_t magRegisterValues[8];

    // printf("intent\n");

    if(!magDataReady){
        SpiMultiByteReadMPU( EXT_SENS_DATA_00, magRegisterValues, 1);
        // printf("%i \n", magRegisterValues[0]);
        if( (magRegisterValues[0]&0b00000001) == 0b00000001 || magRegisterValues[0] == 2){
            // printf("magDataReady @ %i\n",magTime.Getlooptime());
            magDataReady = true;
        }else{
            return false;
        }
    } 

    if(!readPrepared){
        EnableReadingFromMag(AK8963_HXL,7);
        // magTime.SetTime();
        readPrepared = true;
        // printf("reads enabled\n");
        return false;
    }

    if(readPrepared){
        SpiMultiByteReadMPU( EXT_SENS_DATA_00, magRegisterValues, 7); //read HXL-HZH Measurement Data Registers and read ST2 Register to check for magnetic sensor overflow and to release data protection of Measurement Data Registers during read

        // magTime.SetTime();

        // if(magRegisterValues[0] == 0){
        //     printf("Mag: Keine werte bis jetzt\n");
        // }

        RegisterFusion(magRegisterValues, magValues, 3,LITTLEENDIAN);
        
        if((magRegisterValues[6]&0b00001000) == 0b00001000){                        // check Status 2 for magnetic sensor overflow
            // printf("Magnetic overflow occured\n"); 
            return false;
        }

        mag[X] = ((float)magValues[X] - magBias[X]) * magScale[X]; 
        mag[Y] = -((float)magValues[Y] - magBias[Y]) * magScale[Y];
        mag[Z] = ((float)magValues[Z] - magBias[Z]) * magScale[Z];

        // magAngle = WrapAroundAngle( (180/3.141 * atan2(mag[X],mag[Y])) );

        // magFactoryScaled[X] = ((float)magValues[X]*sensitivityAdjustment[0]); 
        // magFactoryScaled[Y] = ((float)magValues[Y]*sensitivityAdjustment[1]);
        // magFactoryScaled[Z] = ((float)magValues[Z]*sensitivityAdjustment[2]);

        // printf("%i \t",magTime.GetTimePassed());

        EnableReadingFromMag(AK8963_ST1,1);

        magDataReady = false;
        readPrepared = false;
        // printf("%d %d\n",magDataReady,readPrepared);
        // printf("return true\n");
        return true;
    }    

    return false;
}

void MPU9250::EnableMag(){
    SpiWriteSingleByteMPU( USER_CTRL, USER_CTRL_FIFO_EN|I2C_MST_EN);          // Enable the I2C Master I/F module; pins ES_DA and ES_SCL are isolated from pins SDA/SDI and SCL/ SCLK BIT[5]=1

    SpiWriteSingleByteMPU( I2C_MST_CTRL, 9);                // I2C Master Clock Speed 400kHz 

    SpiWriteSingleByteMPU( I2C_SLV0_ADDR, AK8963_I2C_ADDR); // Physical address of I2C slave 0, Transfer is a write BIT[7]=0 => write to magnetometer directly from the host (MPU9250) slave address for the AK8963 is 0X0C (MPU-9250 Product Specification Revision 1.0: Pass-Through mode)
    SpiWriteSingleByteMPU( I2C_SLV0_REG, AK8963_CNTL2);     // I2C slave 0 register address from where to begin data transfer => CNTL2 Register
    SpiWriteSingleByteMPU( I2C_SLV0_DO, SOFT_RESET);        // Data out when slave 0 is set to write (BIT[7]=0 I2C_SLV0_ADDR) => Soft reset BIT[0] (bit auto clears after one clock cycle so can't be checked)
    usleep(1000);
}

void MPU9250::SetMagInPowerDownMode(){
    do {
        SpiWriteSingleByteMPU( I2C_SLV0_ADDR, AK8963_I2C_ADDR); // Physical address of I2C slave 0, Transfer is a write BIT[7]=0 => write to magnetometer directly from the host (MPU9250) slave address for the AK8963 is 0X0C (MPU-9250 Product Specification Revision 1.0: Pass-Through mode)
        SpiWriteSingleByteMPU( I2C_SLV0_REG, AK8963_CNTL1);     // I2C slave 0 register address from where to begin data transfer
        SpiWriteSingleByteMPU( I2C_SLV0_DO, OUTPUT_16_BIT|POWER_DOWN_MODE);        // Power-down mode BIT[3:0] = 0000, 16-Bit output BIT[4]=1
        usleep(1000);
        EnableReadingFromMag(AK8963_CNTL1,1);
        usleep(1000); // wait for sample (1kHz Samplerate)  
        if(SpiSingleByteReadMPU(EXT_SENS_DATA_00) == (OUTPUT_16_BIT|POWER_DOWN_MODE)){
            // printf("Success set AK8963_CNTL1 to Power-down mode\n");
            usleep(100000);
            repeat = false;
        }
        else{
            printf("Fail set AK8963_CNTL1 to Power-down mode! AK8963_CNTL1 is decimal value %i instead of %i \n",SpiSingleByteReadMPU( EXT_SENS_DATA_00),OUTPUT_16_BIT|POWER_DOWN_MODE);
            // TestReadMagDeviceIDRegister();
            usleep(100000);
            repeat = true;
        }    
    } while(repeat);
}

void MPU9250::SetMagInFuseRomAccessMode(){
    do {
        SpiWriteSingleByteMPU( I2C_SLV0_ADDR, AK8963_I2C_ADDR); // Physical address of I2C slave 0, Transfer is a write BIT[7]=0 => write to magnetometer directly from the host (MPU9250) slave address for the AK8963 is 0X0C (MPU-9250 Product Specification Revision 1.0: Pass-Through mode)
        SpiWriteSingleByteMPU( I2C_SLV0_REG, AK8963_CNTL1);     // I2C slave 0 register address from where to begin data transfer
        SpiWriteSingleByteMPU( I2C_SLV0_DO, OUTPUT_16_BIT|FUSE_ROM_ACCESS_MODE);        // Fuse ROM access mode BIT[3:0] = 1111, 16-Bit output BIT[4]=1
        usleep(1000);
        EnableReadingFromMag(AK8963_CNTL1,1);
        usleep(1000); // wait for sample (1kHz Samplerate)
        if(SpiSingleByteReadMPU( EXT_SENS_DATA_00) == (OUTPUT_16_BIT|FUSE_ROM_ACCESS_MODE)){
            // printf("Success set AK8963_CNTL1 to Fuse ROM access mode\n");
            usleep(100000);
            repeat = false;
        }
        else{
            printf("Fail set AK8963_CNTL1 to Fuse ROM access mode! AK8963_CNTL1 is decimal value %i instead of %i \n",SpiSingleByteReadMPU( EXT_SENS_DATA_00),OUTPUT_16_BIT|FUSE_ROM_ACCESS_MODE);
            usleep(100000);
            repeat = true;
        }    
    } while(repeat);
}

void MPU9250::GetMagASAValues(){
    uint8_t ASA[3]={0};

    EnableReadingFromMag(AK8963_ASAX,3); // Enable reading data from ASA (Axis Sensitivity Adjustment) registers
    usleep(1000); // wait for sample (1kHz Samplerate)  
    SpiMultiByteReadMPU( EXT_SENS_DATA_00,ASA,3);


    for(int i=0; i<3; i++) {
        sensitivityAdjustment[i]=((float)(ASA[i]-128)/256)+1;
        // printf("%f\n",sensitivityAdjustment[i]);
    }
}

void MPU9250::SetMagInContinuosMeasurementMode(){
    do {
        SpiWriteSingleByteMPU(I2C_SLV0_ADDR, AK8963_I2C_ADDR); // Physical address of I2C slave 0, Transfer is a write BIT[7]=0 => write to magnetometer directly from the host (MPU9250) slave address for the AK8963 is 0X0C (MPU-9250 Product Specification Revision 1.0: Pass-Through mode)
        SpiWriteSingleByteMPU(I2C_SLV0_REG, AK8963_CNTL1);     // I2C slave 0 register address from where to begin data transfer
        SpiWriteSingleByteMPU(I2C_SLV0_DO, OUTPUT_16_BIT|CONTINUOUS_MEASUREMENT_MODE_2); // Output bit setting: 16-bit output BIT[4]=1, Continuous measurement mode 2 (sample rate 100 Hz) BIT[3:0] = 0110
        usleep(1000);
        EnableReadingFromMag(AK8963_CNTL1,1);
        usleep(1000); // wait for sample (1kHz Samplerate)  
        if(SpiSingleByteReadMPU(EXT_SENS_DATA_00) == (OUTPUT_16_BIT|CONTINUOUS_MEASUREMENT_MODE_2)){
            // printf("Success set AK8963_CNTL1 to 16-bit output and to continuous measurement mode 2\n");
            usleep(100000);
            repeat = false;
        }
        else{
            printf("Fail set AK8963_CNTL1 to 16-bit output and to continuous measurement mode 2! AK8963_CNTL1 is decimal value %i instead of %i \n",SpiSingleByteReadMPU( EXT_SENS_DATA_00),OUTPUT_16_BIT|CONTINUOUS_MEASUREMENT_MODE_2);
            usleep(100000);
            repeat = true;
        }
    } while(repeat);
}

void MPU9250::TestReadMagDeviceIDRegister(){
    EnableReadingFromMag(AK8963_WIA,1);
    usleep(1000); // wait for sample (1kHz Samplerate)  
        if(SpiSingleByteReadMPU( EXT_SENS_DATA_00) == 0x48){
            // printf("Success read AK8963_WIA Register\n");
            usleep(100000);
        }
        else{
            printf("Fail read AK8963_WIA Register! AK8963_WIA read is decimal value %i instead of %i \n",SpiSingleByteReadMPU( EXT_SENS_DATA_00),0x48);
            usleep(100000);
        }
}

void MPU9250::EnableReadingFromMag(uint8_t i2cSlaveRegisterAdress, uint8_t nBytes){
    SpiWriteSingleByteMPU( I2C_SLV0_ADDR,AK8963_I2C_ADDR|TRANSFER_IS_A_READ);   // Transfer is a read  BIT[7]=1 (READ_BITMASK), Physical address of I2C slave 0 (BARO)
    SpiWriteSingleByteMPU( I2C_SLV0_REG, i2cSlaveRegisterAdress);               // I2C slave 0 register address from where to begin data transfer
    SpiWriteSingleByteMPU( I2C_SLV0_CTRL, ENABLE_READING|nBytes);               // Enable reading data from this slave at the sample rate and storing data in EXT_SENS_DATA_00 (10000000), Read 1 bytes from the magnetometer (00000001)
    // usleep(1000);                                                       // wait for sample (1kHz Samplerate)  
}

uint8_t MPU9250::WhoAmI(){
    return SpiSingleByteReadMPU(WHO_AM_I);
}

float MPU9250::WrapAroundAngle(float angle){
    return angle - 360.0 * floor(angle / 360);
}

void MPU9250::ReadAccel(){
    uint8_t accelRegisterValues[6];
    SpiMultiByteReadMPU( ACCEL_XOUT_H, accelRegisterValues, 6);
    RegisterFusion(accelRegisterValues, accelValues, 3, BIGENDIAN);

    accel[X] = (float)accelValues[X];
    accel[Y] = (float)accelValues[Y];
    accel[Z] = (float)accelValues[Z]; 
}

void MPU9250::ReadGyro(){
    uint8_t gyroRegisterValues[6];
    SpiMultiByteReadMPU( GYRO_XOUT_H, gyroRegisterValues, 6);
    RegisterFusion(gyroRegisterValues, gyroValues, 3, BIGENDIAN);

    gyro[X] = (float)gyroValues[X];
    gyro[Y] = (float)gyroValues[Y];
    gyro[Z] = (float)gyroValues[Z]; 
}