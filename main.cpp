#include <stdio.h>
#include <pigpio.h>	
#include <unistd.h> // usleep()
#include <fstream> 	// blackbox

#include "Control.h"
#include "ms5611.h"
#include "timefunctions.h"
#include "userinterface.h"
#include "ppm.h"
#include "ADS1115.h"
#include "MPU9250.h"
#include "constants.hpp"
#include "complementary_attitude.h"
#include "complementary_altitude.h"
#include "quaternions.h"

using namespace std;

extern uint16_t ppm[8];

#define LOOPTIME 2500000 //in ns

bool blackboxOn = true;
bool on = true;
bool autostart = false;
bool holdAltitude = false;

MS5611 baro;
MPU9250 imu;
PCA9685 pwm;
ADS1115 adc;
control control;
AttitudeFusion fusion;
AltitudeFusion alti;
ofstream blackbox;
Userinterface Userinterface;
Timefkt mainlooptime(LOOPTIME);

PID pitchInnerControlLoop(59,0,0); 	// 59,0,0
PID rollInnerControlLoop(59,0,0); 	// 59,0,0
PID yawInnerControlLoop(0,0,0); 	// 20,0,0
PID altitudeInnerControlLoop(0,0,0);

PID pitchOuterControlLoop(0.01,0,0); 	// 0.01,0,0
PID rollOuterControlLoop(0.01,0,0); 	// 0.01,0,0
PID yawOuterControlLoop(0,0,0); 		// 0.01,0,0
PID altitudeOuterControlLoop(0,0,0);


int main(){  

	//===============================================PPM setup / active PPM listening 
		setupPPMGPIO();
		usleep(100000); //shoud be 20000 but first values are wrong

	//===============================================Initialise LEDs
		gpioWrite(24,1);
		gpioPWM(25,225);

	//===============================================PWMdriver setup
		pwm.Setup();
		// pwm.ServoTrim();
		// pwm.TestPWM();

	//===============================================Initialize Baro
		baro.Initialize();
		baro.SetBaroOSR(ADC_OSR_2048);

	//===============================================Initialize Imu
		imu.Initialize();

	//===============================================Get calibration data from flightomato.ini file
		if(!imu.GetCalibration()){ 
		return -1;
		}

	//===============================================Initialize blackbox
		if(blackboxOn){
			blackbox.open("blackbox.csv");
			printf("\nOpening blackbox\n");
			if (!blackbox.is_open()){
			  return -1;
			}
		}
		blackbox <<  "dT Imu" <<","<< "Gyro X" <<","<< "Gyro Y" <<","<< "Gyro Z" <<","<< "Accel X" <<","<< "Accel Y" <<","<< "Accel Z" <<",";
		blackbox <<  "Magnet X" <<","<< "Magnet Y" <<","<< "Magnet Z" << ","<< "Euler Winkel X" << ","<< "Euler Winkel Y" << ","<< "Euler Winkel Z" << ","<< "dt Barometer" <<","<< "Barometer Höhe" <<",";
		blackbox <<  "globale Beschl. X" <<","<< "globale Beschl. Y" <<","<< "globale Beschl. Z korrigiert" <<","<< "vert. Geschw. fusioniert" <<","<< "Höhe fusioniert" <<",";
		blackbox <<  "Vorgabe Winkel Pitch" <<","<< "Vorgabe Winkel Roll" <<","<< "Vorgabe Winkel Yaw" <<","<< "Vorgabe Höhe" <<","<< "Vorgabe Winkelbeschl. Pitch" <<","<< "Vorgabe Winkelbeschl. Roll" <<","<< "Vorgabe Winkelbeschl. Yaw" <<","<< "Vorgabe vertikale Geschw." << ",";
		blackbox <<  "Servo1 (rear)" <<","<< "Servo2 (front)" << "," << "Servo3 (right)" << "," << "Servo4 (left)" << "Motor1 (rear)" <<","<< "Motor2 (front)" << "," << "Motor3 (right)" << "," << "Motor4 (left)" << "\n";

	//===============================================Userinterfaces for arming UAS. Dependent on bool autostart. Gets atmospheric pressure for altitude reference
		Userinterface.GetRCSwitch(); // Gets rc switch state used as start and kill switch 
		uint16_t startPulsewidth[8]; // Set pulse width for servos and motors on zero output
		startPulsewidth[0] = 290;
		startPulsewidth[1] = 308;
		startPulsewidth[2] = 318;
		startPulsewidth[3] = 313;

		startPulsewidth[4] = 204;
		startPulsewidth[5] = 204;
		startPulsewidth[6] = 204;
		startPulsewidth[7] = 204;
		pwm.SetPWM(startPulsewidth);

    //=============================================Start reading enable fifo
    imu.EnableFifoWrites(WRITE_GYRO|WRITE_ACCEL); // Enable writing of GYRO and ACCEL measuremets on FIFO (call directy before loop to avoid FIFO overflow) 
    usleep(10000);                // avoid fifo read before first sample (1kHz Sample-Rate)

    //
		if(autostart){
			while(Userinterface.ProgramstartViaRC()){
		  		baro.Calibrate();
			}
		}else{
			printf("        Start Drone        - s\n");
			printf("     Calibrate Motors      - m\n");
			Userinterface.DisableConsoleBuffering();
			do{
			  baro.Calibrate();
        imu.GetAccelGyroData();

      //---------------------update Fusion
        fusion.predict(imu.timestamp, imu.gyro);
        fusion.updateAccel(imu.accel);
        usleep(1000);

      //---------------------read Magnetometer
        if(imu.GetMagData()){
         // fusion.updateMagnet(imu.mag);
         // printf("pitch: %+3.4f \t roll: %+3.4f \t yaw: %+3.4f \n",fusion.attiEuli[Y],fusion.attiEuli[X],fusion.attiEuli[Z]);
        }

			}while(!Userinterface.ProgramstartViaSsh(startPulsewidth));
		} 

	//===============================================setting target angles for attitude control
      // fusion.Ini(imu.accel,imu.mag,  imu.pitchMountingAngle, imu.rollMountingAngle, 0);
	    pitchOuterControlLoop.Refset(imu.pitchMountingAngle);
	    rollOuterControlLoop.Refset(imu.rollMountingAngle);
	    yawOuterControlLoop.Refset(fusion.attiEuli[Z]);                                         


	// //===============================================Kickstart some sensors before loop==============
		baro.StartBaro();                             // Start Pressure Conversation

	//===============================================Update time for loop-time equalization
		mainlooptime.SetTime();


	//===============================================Main loop
	while (on){
	  	
		//===============================================Battery Voltage check
	    	adc.CheckBatteryVoltage();
	      	// adc.PrintADCvalues();

	    //===============================================Time Equalization
	    	mainlooptime.Timeequalize();
	    	// printf("%ld \n",mainlooptime.Getlooptime());
	      	// mainlooptime.Getmaxlooptime();

	    //===============================================Determine attitude  	

	    //---------------------read Accelerometer und Gyroscope
	      	imu.GetAccelGyroData();

	    //---------------------update Fusion
	      	fusion.predict(imu.timestamp, imu.gyro);
	      	fusion.updateAccel(imu.accel);

	    //---------------------read Magnetometer
			if(imu.GetMagData()){
			   // fusion.updateMagnet(imu.mag);
			   // printf("pitch: %+3.4f \t roll: %+3.4f \t yaw: %+3.4f \n",fusion.attiEuli[Y],fusion.attiEuli[X],fusion.attiEuli[Z]);
			}
			
		//---------------------calculate euler angles
			fusion.calculateEuli();

		    // printf("pitch: %+3.4f \t roll: %+3.4f \t yaw: %+3.4f \n",imu.pitchMountingAngle - fusion.attiEuli[Y], imu.rollMountingAngle  - fusion.attiEuli[X],fusion.attiEuli[Z]);
		    // printf("accelx: %+.4f \t accely: %+.4f \t accelz: %+.4f   ",imu.accel[X],imu.accel[Y],imu.accel[Z]);
		    // printf("magnetx: %+3.4f \t magnety: %+3.4f \t magnetz: %+3.4f",imu.mag[X],imu.mag[Y],imu.mag[Z]);
		    // printf("gyrox: %+.4f gyroy: %+.4f gyroz: %+.4f",imu.gyr[X],imu.gyr[Y],imu.gyr[Z]);


	    //===============================================Feedback Control Attitude======================================

	    //---------------------attitude outer loops
			// if (ppm[3]>1508){
			//   yawOuterControlLoop.Refchange((float)(ppm[3]-1500)/(float)500);
			// }else if (ppm[3]<1492){
			//   yawOuterControlLoop.Refchange((float)(ppm[3]-1500)/(float)500);
			// }

			pitchOuterControlLoop.CalcPID(fusion.attiEuli[Y],1);
			rollOuterControlLoop.CalcPID(fusion.attiEuli[X],1);
			yawOuterControlLoop.CalcPID(fusion.attiEuli[Z],1);
			// printf("%f %f %f\n", PIDfeedForwardPitch.output, PIDfeedForwardRoll.output, PIDfeedForwardYaw.output);


	    //---------------------attitude inner loops
			pitchInnerControlLoop.Refset(pitchOuterControlLoop.output);
			rollInnerControlLoop.Refset(rollOuterControlLoop.output);
			yawInnerControlLoop.Refset(yawOuterControlLoop.output);

			pitchInnerControlLoop.CalcPID(imu.gyro[Y], imu.timestamp);
			rollInnerControlLoop.CalcPID(imu.gyro[X], imu.timestamp);
			yawInnerControlLoop.CalcPID(imu.gyro[Z], imu.timestamp);

		//---------------------Tuning of kp,ki,kd values
			pitchInnerControlLoop.PidTune();
			rollInnerControlLoop.PidTune();
			// yawInnerControlLoop.PidTune();

			// pitchOuterControlLoop.PidTune();
			// rollOuterControlLoop.PidTune();
			// yawOuterControlLoop.PidTune();


	    //===============================================Feedback Control Altitude
			alti.predict(imu.accel, fusion.q_lg, imu.timestamp);
			if(baro.ReadBaro()){
			// printf("baro altitude: %f P: %i Cali: %i\n",baro.altitude,baro.P,baro.calibrationPressure);
			alti.updateBaro(baro.altitude, fusion.q_lg ,baro.timestamp);
			}
			if(ppm[5] > 1800){
				if(!holdAltitude){
					altitudeOuterControlLoop.Refset(alti.altitude);
					holdAltitude = true;
				}
				altitudeOuterControlLoop.CalcPID(alti.altitude, 1);
				altitudeInnerControlLoop.Refset(altitudeOuterControlLoop.output);
				altitudeInnerControlLoop.CalcPID(alti.velocity, imu.timestamp);
			}else{
				altitudeInnerControlLoop.output = 0;
				holdAltitude = false;
			}


	    //===============================================Servo position and motor throttle calculation and output
			control.RcConvert();
			control.CalcPWM(pitchInnerControlLoop.output, rollInnerControlLoop.output, yawInnerControlLoop.output, altitudeInnerControlLoop.output, adc.voltage);

	    //---------------------output PWM signal to servos and motors
			pwm.SetPWM(control.pulseWidth);
			// printf("%i %i %i %i %i %i %i %i\n",control.pulseWidth[0],control.pulseWidth[1],control.pulseWidth[2],control.pulseWidth[3],control.pulseWidth[4],control.pulseWidth[5],control.pulseWidth[6],control.pulseWidth[7] );

	    //=============================================== Print into blackbox
			if(blackboxOn){
				if(ppm[5] > 1800){
				}else{
				}
				blackbox <<  imu.timestamp <<","<< imu.gyro[X] <<","<< imu.gyro[Y] <<","<< imu.gyro[Z] <<","<< imu.accel[X] <<","<< imu.accel[Y] <<","<<imu.accel[Z] <<",";
				blackbox <<  imu.mag[X] <<","<< imu.mag[Y] <<","<< imu.mag[Z] <<","<< imu.rollMountingAngle - fusion.attiEuli[X] << ","<< imu.pitchMountingAngle - fusion.attiEuli[Y] << ","<< fusion.attiEuli[Z] << ","<<baro.timestamp <<","<< baro.altitude <<",";
				blackbox <<  alti.globalAccel[X] <<","<< alti.globalAccel[Y] <<","<< alti.globalAccel[Z] <<","<< alti.velocity <<","<< alti.altitude <<",";
				blackbox <<  pitchOuterControlLoop.w <<","<< rollOuterControlLoop.w << "," << yawOuterControlLoop.w << "," << altitudeOuterControlLoop.w << "," << pitchInnerControlLoop.w << "," << rollInnerControlLoop.w << "," << yawInnerControlLoop.w << "," << altitudeInnerControlLoop.w <<",";
				blackbox <<  "," <<control.pulseWidth[0] <<","<< control.pulseWidth[1] << "," << control.pulseWidth[2] << "," << control.pulseWidth[3] << "," <<control.pulseWidth[4] <<","<< control.pulseWidth[5] << "," << control.pulseWidth[6] << "," << control.pulseWidth[7] <<"\n";
			}

	    //=============================================== Emergency OFF
			on = Userinterface.Emergencyoff(startPulsewidth);
		
	}
	//===============================================Print battery voltage, turn of LEDs and close the blackbox file before exeting program
	adc.PrintADCvalues();
	gpioWrite(24,1);
	gpioWrite(25,1);
	if(blackboxOn){
	blackbox.close();
	}
	return 0;
}
