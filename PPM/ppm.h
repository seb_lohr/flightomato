#ifndef PPM_H
#define PPM_H

#define SAMPLINGRATE 5      // 1 microsecond (can be 1,2,4,5,10)
#define PPMINPUTGPIO 4      // PPM input GPIO PIN

 
void savePPM(int gpio, int level, uint32_t tick);
void setupPPMGPIO();

#endif