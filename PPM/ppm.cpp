#include <pigpio.h>
#include "timefunctions.h"

#include "ppm.h"

uint16_t ppm[8];
unsigned int currentChannel = 0;
unsigned int previousTick;
unsigned int deltaTime;
unsigned int ppmSyncLength  = 4000;   // Length of PPM sync pause

using namespace std;

//setting up of GPIO4 which is connected to the ppm-input from the rc receiver using the pigpio library
void setupPPMGPIO(){
  gpioCfgClock(SAMPLINGRATE, PI_DEFAULT_CLK_PERIPHERAL, 0);  //last parameter is deprecated now 
  gpioInitialise();
  previousTick = gpioTick();
  gpioSetAlertFunc(PPMINPUTGPIO, savePPM);
}

//Calculates the time since the last rising edge of the ppm-input
void savePPM(int gpio, int level, uint32_t tick){
   if (level == 0) {   
       deltaTime = tick - previousTick;
       previousTick = tick;

       if (deltaTime >= ppmSyncLength) {
           currentChannel = 0;
       }
       else
          if (currentChannel < 8){
            ppm[currentChannel] = deltaTime;
          currentChannel++;  
          }
   }
}