#include "I2C.h"

using namespace std;

int fd;


void OpenI2Cdevice(){
    fd = open("/dev/i2c-1", O_RDWR);
    if(fd < 0){
        printf("could not open I2C device\n");
    } 
}

void I2cWriteBytes(uint8_t deviceAddress, uint8_t registerAddress, uint8_t *data, uint8_t numberOfBytes){
    uint8_t writeBuffer[numberOfBytes + 1];
    writeBuffer[0] = registerAddress;
    memcpy(writeBuffer+1, data, numberOfBytes);
    if (ioctl(fd, I2C_SLAVE, deviceAddress) < 0) {
        printf("failed to set slave address\n");
    } 
    if (write(fd, writeBuffer, numberOfBytes+1) < 0) {
        printf("I2C write failed\n");
    }
}

void I2cWriteWord(uint8_t deviceAddress, uint8_t registerAddress, uint16_t data){
    uint8_t writeBuffer[3];
    writeBuffer[0] = registerAddress;
    writeBuffer[1] = data >> 8;  // put MSB on buffer
    writeBuffer[2] = data;       // put LSB on buffer
    if (ioctl(fd, I2C_SLAVE, deviceAddress) < 0) {
        printf("failed to set slave address\n");
    } 
    if (write(fd, writeBuffer, 3) < 0) {
        printf("I2C write failed\n");
    }
}

void I2cReadBytes(uint8_t deviceAddress, uint8_t registerAddress, uint8_t *data, uint8_t numberOfBytes){
    struct i2c_rdwr_ioctl_data packets;
    struct i2c_msg messages[2];

    messages[0].addr  = deviceAddress;
    messages[0].flags = 0;
    messages[0].len   = 1;
    messages[0].buf   = &registerAddress;

    messages[1].addr  = deviceAddress;
    messages[1].flags = I2C_M_RD; // I2C_M_NOSTART
    messages[1].len   = numberOfBytes;
    messages[1].buf   = data;

    packets.msgs      = messages;
    packets.nmsgs     = 2;

    if(ioctl(fd, I2C_RDWR, &packets) < 0) {
        printf("Unable to send data\n");
    }
}

int16_t I2cReadWord(uint8_t devAddr, uint8_t regAddr){
    uint8_t data[2];
    I2cReadBytes(devAddr,regAddr, data, 2);
    return (int16_t)(((uint16_t)data[0] << 8) | (uint16_t)data[1]); 
}


