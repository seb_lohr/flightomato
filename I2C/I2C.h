#ifndef I2C_H
#define I2C_H

#include <stdio.h>	//strerror
#include <stdlib.h>
#include <stdint.h>	//uint types
#include <string.h>	//strerror
#include <unistd.h>	//sleep
#include <math.h>	//roundf
#include <fcntl.h>	//open I2C device
#include <errno.h>	//strerror(errno)
#include <sys/ioctl.h>	//ioctl
#include <linux/i2c.h>
#include <linux/i2c-dev.h>	//I2C_SLAVE	
#include <iostream> //cout 

void OpenI2Cdevice();
void I2cWriteBytes(uint8_t deviceAddress, uint8_t registerAddress, uint8_t *data, uint8_t numberOfBytes);
void I2cReadBytes(uint8_t deviceAddress, uint8_t registerAddress, uint16_t *data, uint8_t numberOfWords);
void I2cWriteWord(uint8_t deviceAddress, uint8_t registerAddress, uint16_t data);
int16_t I2cReadWord(uint8_t deviceAddress, uint8_t registerAddress);

#endif