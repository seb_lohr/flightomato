#include <pigpio.h> // gpioWrite()

#include "I2C.h"
#include "ppm.h"
#include "ADS1115.h"
#include "timefunctions.h"

using namespace std;

Timefkt adsTime;

ADS1115::ADS1115() {
    OpenI2Cdevice();
    SetDataRate(DATA_RATE_475_SPS);
    SetOperatingMode(OPERATING_MODE_CONTINUOUS);
    SetGainAmplifier(GAIN_AMPLIFIER_2048);
    SetMultiplexer(MULTIPLEXER_AIN1_AND_GND);
    WriteOnConfigRegister();
    ledStatus = 1;
    CritAmpHour = 0.8 * BATTERY_AMP_HOUR * 360 / 1000;
}

void ADS1115::SetDataRate(uint16_t rate){
    DataRate = rate;
}

void ADS1115::SetOperatingMode(uint16_t mode){
    OperatingMode = mode;
}

void ADS1115::SetMultiplexer(uint16_t mux){
    Multiplexer = mux;
}


void ADS1115::SetGainAmplifier(uint16_t gain){
    GainAmplifier = gain;
    switch(gain){
        case GAIN_AMPLIFIER_6144:
            gainFactor = 6.144;
        break;
        case GAIN_AMPLIFIER_4096:
            gainFactor = 4.096;
        break;
        case GAIN_AMPLIFIER_2048:
            gainFactor = 2.048;
        break;
        case GAIN_AMPLIFIER_1024:
            gainFactor = 1.024;
        break;
        case GAIN_AMPLIFIER_0512:
            gainFactor = 0.512;
        break;
        case GAIN_AMPLIFIER_0256:
            gainFactor = 0.256;
        break;
    }
}

void ADS1115::StartSingleShot(){
    uint16_t configRegisterSatusBitmask = 0;
    configRegisterSatusBitmask = DataRate | OperatingMode | Multiplexer | GainAmplifier | START_SINGLE_SHOT;
    I2cWriteWord(SLAVE_ADDRESS_ADS1115, ADS_CONFIG_REGISTER, configRegisterSatusBitmask);
}

void ADS1115::WriteOnConfigRegister(){
    uint16_t configRegisterSatusBitmask = 0;
    configRegisterSatusBitmask = DataRate | OperatingMode | Multiplexer | GainAmplifier;
    I2cWriteWord(SLAVE_ADDRESS_ADS1115, ADS_CONFIG_REGISTER, configRegisterSatusBitmask);
}



void ADS1115::CheckBatteryVoltage(){
    waitCounter += 1;
    if(waitCounter == 200){
        SetOperatingMode(OPERATING_MODE_SINGLE_SHOT);
        SetMultiplexer(MULTIPLEXER_AIN0_AND_GND);
        WriteOnConfigRegister();
        return;
    }
    else if(waitCounter == 201){
        StartSingleShot();
        return;
    }
    else if(waitCounter == 203){
        waitCounter = 0;
        voltage = (I2cReadWord(SLAVE_ADDRESS_ADS1115, ADS_CONVERSION_REGISTER)/(float)32768*gainFactor*1.6*10);
        // printf("voltage: %.3f V\n", voltage );
        SetOperatingMode(OPERATING_MODE_CONTINUOUS); // Set operating mode back to continous reading of current
        SetMultiplexer(MULTIPLEXER_AIN1_AND_GND);
        WriteOnConfigRegister();
    }
    else{
        current = I2cReadWord(SLAVE_ADDRESS_ADS1115, ADS_CONVERSION_REGISTER)/(float)32768*gainFactor*1.6*10;
        // printf("current: %.3f A\n", current);
        timestamp = (double)adsTime.Getlooptime() / 1000000000;                            //timestamp of when fifo data was collected in seconds
        ampHour += timestamp * current;
    }
    // printf("voltage:%f\n",voltage );
    if(voltage<6.8 || ampHour>CritAmpHour){
        if (waitCounter == 0 || waitCounter == 100){
            ledStatus=1-ledStatus;
            gpioWrite(LEDAMBER,ledStatus);
        }  
    }
}




void ADS1115::PrintADCvalues(){
    printf("Battery voltage: %.3f V\n", voltage);
    printf("Battery capacity used: %.7f mAh\n", ampHour/360);
    // printf("Current: %.3f in A\n",current);
}