#ifndef ADS1115_H
#define ADS1115_H

/*-------------------------------------------------------------------------------------------------
battery capacity in amph
-------------------------------------------------------------------------------------------------*/

#define BATTERY_AMP_HOUR 1900 //in mAh



/*-------------------------------------------------------------------------------------------------
I2C slave address - ADS1115
-------------------------------------------------------------------------------------------------*/

#define SLAVE_ADDRESS_ADS1115    0b1001000 // ADDR pin, that configures the I2C address of the device is connected to ground so ADS1115 slave address is 1001000

/*-------------------------------------------------------------------------------------------------
Register address pointer - ADS1115
-------------------------------------------------------------------------------------------------*/

#define ADS_CONVERSION_REGISTER     0b00 
#define ADS_CONFIG_REGISTER         0b01 
#define ADS_LO_THRESH_REGISTER      0b10 
#define ADS_HI_THRESH_REGISTER      0b11 

/*-------------------------------------------------------------------------------------------------
Bitmasks config register - ADS1115
-------------------------------------------------------------------------------------------------*/

#define DATA_RATE_8_SPS             0b00000000
#define DATA_RATE_16_SPS            0b00100000
#define DATA_RATE_32_SPS            0b01000000
#define DATA_RATE_64_SPS            0b01100000
#define DATA_RATE_128_SPS           0b10000000 // default
#define DATA_RATE_250_SPS           0b10100000
#define DATA_RATE_475_SPS           0b11000000
#define DATA_RATE_860_SPS           0b11100000

#define OPERATING_MODE_SINGLE_SHOT  0b100000000
#define OPERATING_MODE_CONTINUOUS   0b000000000

#define GAIN_AMPLIFIER_6144         0b000000000000 
#define GAIN_AMPLIFIER_4096         0b001000000000 
#define GAIN_AMPLIFIER_2048         0b010000000000 // default
#define GAIN_AMPLIFIER_1024         0b011000000000 
#define GAIN_AMPLIFIER_0512         0b100000000000 
#define GAIN_AMPLIFIER_0256         0b101000000000 

#define MULTIPLEXER_AIN0_AND_GND    0b100000000000000
#define MULTIPLEXER_AIN1_AND_GND    0b101000000000000 


#define START_SINGLE_SHOT           0b1000000000000000


/*-------------------------------------------------------------------------------------------------
Raspberry Pi GPIO for LEDs on PXFmini
-------------------------------------------------------------------------------------------------*/

#define LEDAMBER    24
#define LEDBLUE     25
 


class ADS1115 {
    public:
        ADS1115();
        void CheckBatteryVoltage();
        void PrintADCvalues();
        float voltage;
        float current;
        float ampHour;

    private:
        void SetDataRate(uint16_t rate);
        void SetOperatingMode(uint16_t mode);
        void SetGainAmplifier(uint16_t gain);
        void SetMultiplexer(uint16_t mux);
        void StartSingleShot();
        void WriteOnConfigRegister();


        float gainFactor;

        int waitCounter;
        int ledStatus;
        double timestamp;
        float CritAmpHour;

        uint16_t DataRate;
        uint16_t OperatingMode;
        uint16_t Multiplexer;
        uint16_t GainAmplifier;

};

#endif // _ADS1115_H_ 

