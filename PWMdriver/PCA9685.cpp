#include <iostream>
#include <pigpio.h>

#include "PCA9685.h"
#include "I2C.h"

using namespace std;


void PCA9685::Setup(){
  OpenI2Cdevice();      // Open the I2C-bus
  gpioWrite(27,0);      // PWM_EN pin pxfmini connected to OE pin PCA9685 low. All the LED outputs are enabled and follow the output state defined in the LEDn_ON and LEDn_OFF registers with the polarity defined by INVRT bit (MODE2 register).
  SetPWMFrequency(50);  // Set 50Hz PWM Frequency as expected by servos and escs
  EnableAutoIncrement();  
}

void PCA9685::SetPWMFrequency(float frequency) {
   usleep(10000);
   uint8_t prescale = roundf((float)25000000 / (4096 * frequency))  - 1;
   I2cWriteBytes(PCA_SLAVE_ADDRESS, PRE_SCALE, &prescale, 1);
}

// Sets the Auto Increment flag in Mode1 register (AI = 1) so the Control register is automatically incremented after a read or write. 
// This allows reading or writing muliple registers in a row with a single I2C transfer
void PCA9685::EnableAutoIncrement(){
  mode1SatusBitmask |= BITMASK_AI;
  I2cWriteBytes(PCA_SLAVE_ADDRESS, MODE1, &mode1SatusBitmask, 1);
}

// Write array with 8 duty cycles to control the servos and motros on LEDn_ON and LEDn_OFF regsiters for channels LED3 to LED10 
void PCA9685::SetPWM(uint16_t setSignalLowAt[8]) {
  uint16_t setSignalHighAt = 0;
  // for(int i=0;i<8;i++){                            // keep values in range [204,409] -> [1ms,2ms] pulse width
  //   if(length[i]>409){
  //     length[i]=409;
  //   }else if(length[i]<204){
  //     length[i]=204;
  //   }
  // }
  for (int j=0;j<=28;j+=4){                             //split data into higher and lower bytes for I2C transfer
    data[j] = setSignalHighAt & 0xFF;
    data[j+1] = setSignalHighAt >> 8;
    data[j+2] = setSignalLowAt[j/4] & 0xFF;
    data[j+3] = setSignalLowAt[j/4] >> 8; 
   }
   I2cWriteBytes(PCA_SLAVE_ADDRESS, LED3_ON_L,data,32); // write corresponding values on LED_ON and LED_OFF registers for channel 3 to 10. Auto increment must be enabled
}

// funktion allowing servo upright position trimming via user keyboard inputs
void PCA9685::ServoTrim(){
  uint16_t servoHighCounts = 305;
  while(1){
    printf("Type Servo PWM: \n");
    cin.ignore();
    cin >> servoHighCounts;
    pulseWidth[0] = servoHighCounts;
    pulseWidth[1] = servoHighCounts;
    pulseWidth[2] = servoHighCounts;
    pulseWidth[3] = servoHighCounts;
    SetPWM(pulseWidth);
    sleep(1);
  }
}

//Test PCA9685s functionality by changing servo signals from 1ms to 2ms pulse width every second 
//Channels 7 to 10 (PXFmini 5 to 8 ) are kept at 1ms pulse width to leave motors at 0 trottle 
void PCA9685::TestPWM(){
  while(1){
    for(int i=0; i<4; i++){
      pulseWidth[i] = 204; //81 for max rotation   
      cout << pulseWidth[i] << " ";
    }
    for(int i=4; i<8; i++){
      pulseWidth[i] = 204; //81 for max rotation
      cout << pulseWidth[i] << " ";
    }
    cout << "\n";
    SetPWM(pulseWidth);
    sleep(1);
    for(int i=0; i<4; i++){
        pulseWidth[i] = 409;   //491 for max rotation
        cout << pulseWidth[i] << " ";
    }
    for(int i=4; i<8; i++){
        pulseWidth[i] = 204;   //491 for max rotation
        cout << pulseWidth[i] << " ";
    }
    cout << "\n";
    SetPWM(pulseWidth);
    sleep(1);
  }
}
