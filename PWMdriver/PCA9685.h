#ifndef PCA9685_H
#define PCA9685_H

#define PCA_SLAVE_ADDRESS   0x40 // hardware selectable address with Bit-7 fixed to 1 and BIT-[0:6] = 0

/*--------------------------------------------------------------------------------------------------------------
PCA9685 - Register addresses
--------------------------------------------------------------------------------------------------------------*/
#define LED3_ON_L   0x12 // LED3 output and brightness control byte 0
#define PRE_SCALE   0xFE // prescaler to program the PWM output frequency (default is 200 Hz)
#define MODE1       0x00 // Mode register 1

/*--------------------------------------------------------------------------------------------------------------
PCA9685 - Bitmasks
--------------------------------------------------------------------------------------------------------------*/

// Bitmasks MODE1 Register
#define BITMASK_RESTART   0b10000000  // Restart 
#define BITMASK_EXTCLK    0b01000000  // Use EXTCLK pin clock
#define BITMASK_AI        0b00100000  // Register Auto-Increment
#define BITMASK_SLEEP     0b00010000  // Low power mode. Oscillator off

class PCA9685 {
  public:
    void Setup();
    void SetPWM(uint16_t length[8]);
    void ServoTrim();
    void TestPWM();
  private:
    void SetPWMFrequency(float frequency);
    void EnableAutoIncrement();

    uint8_t mode1SatusBitmask;
    uint8_t data[32];
    uint16_t pulseWidth[8];
};

#endif