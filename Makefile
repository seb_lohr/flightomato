CPP = ~/pidev/pitools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++


PIGPIO 				= ../pidev/piroot/usr/include
ARM_LINUX_GNUEABIHF	= ../pidev/piroot/usr/include/arm-linux-gnueabihf/


SPIFOLDER  			= SPI
BAROFOLDER 			= Baro
IMUFOLDER  			= Imu
TIMEPATH 			= Time
PCA9685 			= PWMdriver
CONTROLPATH 		= Control
BAROPATH 			= Baro
PPM 				= PPM
POWERMODULE 		= PowerModule
I2C 				= I2C
FUSIONPATH 			= Fusion
MAVUTILPATH 		= util
CALIPATH			= Imu/ImuCalibration
IMUPATH 			= Imu
USERINTERFACEPATH 	= Userinterface


INCPATH 	=  -I. -I$(CALIPATH) -I$(SPIFOLDER) -I$(TIMEPATH) -I$(IMUFOLDER) -I$(PIGPIO) -I$(PPM) -I$(MAVUTILPATH) -I$(FUSIONPATH) -I$(POWERMODULE) -I$(I2C) -I$(BAROFOLDER) -I$(PCA9685) -I$(CONTROLPATH) -I$(USERINTERFACEPATH) -I$(ARM_LINUX_GNUEABIHF)
CFLAGS  	= -pipe -O2 -Wall
LFLAGS		= -L ~/pidev/piroot/usr/lib -L ~/pidev/piroot/usr/lib/arm-linux-gnueabihf -L ~/pidev/piroot/lib -L ~/pidev/piroot
LIBS    	= -lrt -lpthread -lpigpio


CHECK_IF_DIR_EXISTS = test -d
MK_DIR			    = mkdir -p
OBJECTS_DIR      	= objects


TARGET	    = ../Executable/flightomato
CALITARGET	= ../Executable/calibration
# TARGET     	= executable


MAINOBJECT = main.o
CALIOBJECT = ImuCalibration.o


OBJECTS = \
	spi.o \
	MPU9250.o \
	timefunctions.o \
	PCA9685.o \
	ppm.o \
	ADS1115.o \
	I2C.o \
	complementary_attitude.o \
	complementary_altitude.o \
	Control.o \
	ms5611.o \
	userinterface.o \


MAINOBJ = $(patsubst %,$(OBJECTS_DIR)/%,$(MAINOBJECT))
CALIOBJ = $(patsubst %,$(OBJECTS_DIR)/%,$(CALIOBJECT))
OBJ 	= $(patsubst %,$(OBJECTS_DIR)/%,$(OBJECTS))


DEPENDENCIES = \
	$(SPIFOLDER)/spi.h \
	$(IMUFOLDER)/MPU9250.h \
	$(TIMEPATH)/timefunctions.h \
	$(PCA9685)/PCA9685.h \
	$(CONTROLPATH)/Control.h \
	$(BAROPATH)/ms5611.h \
	$(PPM)/ppm.h \
	$(POWERMODULE)/ADS1115.h \
	$(I2C)/I2C.h \
	$(CALIPATH)/ImuCalibration.h \
	$(FUSIONPATH)/complementary_attitude.h \
	$(FUSIONPATH)/complementary_altitude.h \
	$(MAVUTILPATH)/constants.hpp\
	$(MAVUTILPATH)/maths.h \
	$(MAVUTILPATH)/quaternions.h \
	$(MAVUTILPATH)/vectors.h \
	$(USERINTERFACEPATH)/userinterface.h \


flightomato: $(TARGET)
calibration: $(CALITARGET)


$(TARGET): $(MAINOBJ) $(OBJ)
	@$(CHECK_IF_DIR_EXISTS) ../Executable/ || $(MK_DIR) ../Executable/
	$(CPP) $(LFLAGS) -o $@ $^ $(LIBS)

$(CALITARGET): $(CALIOBJ) $(OBJ)
	@$(CHECK_IF_DIR_EXISTS) ../Executable/ || $(MK_DIR) ../Executable/
	$(CPP) $(LFLAGS) -o $@ $^ $(LIBS)


$(OBJECTS_DIR)/%.o : $(PPM)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(SPIFOLDER)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(BAROFOLDER)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(PCA9685)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(IMUFOLDER)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(POWERMODULE)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(I2C)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(FUSIONPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(TIMEPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(MAVUTILPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(MAVSENSPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(CONTROLPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/%.o : $(USERINTERFACEPATH)/%.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/main.o : main.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)

$(OBJECTS_DIR)/ImuCalibration.o : $(CALIPATH)/ImuCalibration.cpp $(DEPENDENCIES)
	@$(CHECK_IF_DIR_EXISTS) objects/ || $(MK_DIR) objects/
	$(CPP) -std=c++11 -c -o $@ $< $(CFLAGS) $(INCPATH)


.PHONY: clean

clean:
	-rm -r $(OBJECTS_DIR)
	-rm -f $(TARGET)
	-rm -f $(CALITARGET)
