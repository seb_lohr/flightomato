#ifndef USERINTERFACE_H
#define USERINTERFACE_H

class Userinterface{
public:
	void DisableConsoleBuffering();
	char GetConsoleInput();
	void GetRCSwitch();
	bool ProgramstartViaRC();
	bool ProgramstartViaSsh(uint16_t *startPulsewidth);
	bool Emergencyoff(uint16_t *startPulsewidth);
private:
	uint16_t startstopswitch; 
	uint16_t pwm[8];
	char userInput = 0;
	bool firstInputLevel = true;
};


#endif // USERINTERFACE_H