#include <iostream> // uint types
#include <unistd.h> // usleep()
#include <termios.h> //terminal handling functions
#include <sys/ioctl.h>

#include "userinterface.h"
#include "PCA9685.h"

PCA9685 pca;

extern uint16_t ppm[8];

// Disable Linux standard I/O line-buffered behavior for immediate handling of keypresses 
void Userinterface::DisableConsoleBuffering(){
  struct termios  inputOutputSettings;
  tcgetattr(fileno(stdout), &inputOutputSettings); 
  inputOutputSettings.c_lflag &= ~(ICANON);
  tcsetattr(fileno(stdout), TCSANOW, &inputOutputSettings);
}


char Userinterface::GetConsoleInput(){
    int i;
    ioctl(0, FIONREAD, &i);
    if (i <= 0)
        return 0;
    return tolower(getchar());
}

//Gets ppm value of channel 5 of the RC at programstart to detect a system start by using the switch
void Userinterface::GetRCSwitch(){
    startstopswitch = ppm[4];
}

//Function call that checks the state of channel 5 of the RC and starts the Program
bool Userinterface::ProgramstartViaRC(){
    printf("\n\nswitch switch to switch on! \n"); 
    if (startstopswitch - ppm[4] < -500 || startstopswitch - ppm[4] > 500){
        usleep(25000);
        startstopswitch = ppm[4];
        return false;
    }
    return true;
}

//Function to calbrate the ESCs
bool Userinterface::ProgramstartViaSsh(uint16_t *startPulsewidth){
    bool returnValue = false;
    if(firstInputLevel){
        if( (userInput = GetConsoleInput()) != 0){
            switch(userInput){   
                case 's':
                    firstInputLevel = false;
                    printf("\n     Start Drone now       - y\n");
                break;

                case 'm':
                    printf("\n");
                    sleep(1);
                    printf("Connect ESCs to power in: 3\r");
                    fflush(stdout);                             // print everything in the stdout buffer 
                    sleep(1);
                    printf("Connect ESCs to power in: 2\r");
                    fflush(stdout);                             // print everything in the stdout buffer
                    sleep(1);
                    printf("Connect ESCs to power in: 1\r");
                    fflush(stdout);                             // print everything in the stdout buffer
                    sleep(1);
                    printf("Connect ESCs to power in: 0\n");
                    sleep(1);

                    startPulsewidth[4] = 409;
                    startPulsewidth[5] = 409;
                    startPulsewidth[6] = 409;
                    startPulsewidth[7] = 409;
                    pca.SetPWM(startPulsewidth);
                    printf("maximum trottel\n");
                    sleep(7);
                    startPulsewidth[4] = 204;
                    startPulsewidth[5] = 204;
                    startPulsewidth[6] = 204;
                    startPulsewidth[7] = 204;
                    pca.SetPWM(startPulsewidth);
                    printf("minimum trottel\n");
                    sleep(7);
                    firstInputLevel = false;
                    printf("\n     Start Drone now       - y\n");
                break;

                default:
                    printf("s oder m\n");
                    firstInputLevel = true;
                break;
            }   
        }
    }else{
        if( (userInput = GetConsoleInput()) != 0){
            if(userInput == 'y'){
                returnValue = true;
            }
        }
    }
    return returnValue;
}

//detects the state of the kill switch with program launch and turns off the motors if switch is flipped 
bool Userinterface::Emergencyoff(uint16_t *startPulsewidth){
    if (startstopswitch - ppm[4] < -500 || startstopswitch - ppm[4] > 500){
        for(int i=4; i<8; i++){
            startPulsewidth[i] = 204; 
        }
        pca.SetPWM(startPulsewidth);
        return false;
    }
    return true;
}

